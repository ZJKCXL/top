<h1>Správa hodnotitelů</h1>
<?php 
$info_text = "";
//photo
$full_path = $globalgal."/images/users/";
$fullpathico = $globalgal."/images/usersthumbs/";
$time = time();
$file_name  = date("d_m_y",$time)."TZ".$time;
$file_name2 = date("d_m_y",$time)."TZ".$time;
$table_name = "an_tbluser";
$page_name = "anusers";
$deletext = "Uživatel byl smazán.";
$updatext = "Uživatel byl opraven.";
$addtext = "Uživatel byla přidán.";
$jeho = "Uživatele";
 
if(isset($_REQUEST["delete"]) && is_numeric($_REQUEST["delete"]) && $_REQUEST["delete"] > 0)
{
	$delete = $_REQUEST["delete"];

	$query = "Update ".$table_name." set Deleted = 1 WHERE ID = ".$_REQUEST["delete"];
 
	$del_res = mysql_query($query);
	if ($del_res)
	{
		if (mysql_affected_rows($link) > 0)
		{
			$info_text .= "Uživatel byl smazán.";
			$alertype = " alert-success ";
			
		}
		else
		{
			$info_text .= "Uživatel nebyl smazán.";
			$alertype = " alert-danger ";
		}
	}
	else
	{
		$info_text .= "Chyba během mazání uživatele.";
		$alertype = " alert-danger ";
	}
}
 
if(isset($_REQUEST["invalid"]) && is_numeric($_REQUEST["invalid"]) && $_REQUEST["invalid"] > 0)
{
	$invalid = $_REQUEST["invalid"];

	$query = "Update ".$table_name." set UCanLogin = 0 WHERE ID = ".$_REQUEST["invalid"];
 
	$del_res = mysql_query($query);
	if ($del_res)
	{
		if (mysql_affected_rows($link) > 0)
		{
			$info_text .= "Uživatel byl uvolněn z hodnocení <br/>Pokud má stále přiřazená hodnocení, musí se znovu aktivovat a hodnocení uvolnit.";
			$alertype = " alert-success ";
			
		}
		else
		{
			$info_text .= "Uživatel nebyl uvolněn z hodnocení .";
			$alertype = " alert-danger ";
		}
	}
	else
	{
		$info_text .= "Chyba během uvolnění z hodnocení.";
		$alertype = " alert-danger ";
	}
}

 
if(isset($_REQUEST["valid"]) && is_numeric($_REQUEST["valid"]) && $_REQUEST["valid"] > 0)
{
	$invalid = $_REQUEST["valid"];

	$query = "Update ".$table_name." set UCanLogin = 1 WHERE ID = ".$_REQUEST["valid"];
 
	$del_res = mysql_query($query);
	if ($del_res)
	{
		if (mysql_affected_rows($link) > 0)
		{
			$info_text .= "Uživatel byl zařazen mezi aktivní hodnotitele.";
			$alertype = " alert-success ";
			
		}
		else
		{
			$info_text .= "Uživatel nebyl zařazen mezi aktivní hodnotitele.";
			$alertype = " alert-danger ";
		}
	}
	else
	{
		$info_text .= "Chyba během aktivace.";
		$alertype = " alert-danger ";
	}
}

//konec mazani

if (isset($_REQUEST["send"]) && isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"]>0)
{
	if (strlen(trim($_POST["user_login"])))
	{

		$query = "Update ".$table_name." set ";

		$col_query =" ULogin";
		$val_query ="  '".trim(strip_tags($_POST["user_login"]))."'";
		$query .= $col_query." = ".$val_query;

		$col_query =", UName";
		$val_query ="  '".trim(strip_tags($_POST["user_name"]))."'";
		$query .= $col_query." = ".$val_query;

		$col_query =", UEmail";
		$val_query ="  '".trim(strip_tags($_POST["UEmail"]))."'";
		$query .= $col_query." = ".$val_query;

		$col_query =", USurname";
		$val_query ="  '".trim($_POST["user_surname"])."'";
		$query .= $col_query." = ".$val_query;

		$col_query =", UHis";
		$val_query ="  '".trim(@$_POST["UHis"])."'";
		$query .= $col_query." = ".$val_query;
		
		$col_query =", UType";
		$val_query ="  ".intval($_POST["user_level"])."";
		$query .= $col_query." = ".$val_query;
		
            if (isset($image_filename) && strlen($image_filename)>0)
            {
                $col_query = ", UFoto";
                $val_query = " '".$image_filename."'";
                $query .= $col_query." = ".$val_query;
            deletePicture($newsid,'UFoto',$table_name);
            }

		if (strlen($_POST["user_pass"]))
		{
			if ($_POST["user_pass"] == $_POST["user_pass_repeat"])
			{
				$col_query =", UPass";
				$val_query ="  PASSWORD('".trim($_POST["user_pass"])."')";
				$query .= $col_query." = ".$val_query;
			}
		}


		if (isset($_POST["can_login"]))
		{
			$col_query =", UCanLogin";
			$val_query ="  1";
			$query .= $col_query." = ".$val_query;
		}
		else
		{
			$col_query =", UCanLogin";
			$val_query ="  0";
			$query .= $col_query." = ".$val_query;
		}

	 	$query .= " where ID = ".$_REQUEST["newsid"];

		$res = mysql_query($query);

		if ($res && @mysql_affected_rows($link)>0)
		{
			$info_text .= "Uživatel úspěně uložen.";
			$alertype = " alert-success ";

			$priv_query = "Update tblpriviledgeuser set Deleted = 1 where PUUser = ".$_REQUEST["newsid"];
			$priv_res =mysql_query($priv_query);

			if (isset($_REQUEST["privs"]) && is_array($_REQUEST["privs"]))
			{
				foreach ($_REQUEST["privs"] as $priv)
				{
					$ins_query = "Insert into tblpriviledgeuser (PUUser, PUPriviledge) values (".$_REQUEST["newsid"].",".$priv.")";
					$ins_res = mysql_query($ins_query);
				}
			}
		}
		else
		{
			$info_text .= "Uživatel úspěně uložen.";
			$alertype = " alert-success ";

			$priv_query = "Update tblpriviledgeuser_admin set Deleted = 1 where PUUser = ".$_REQUEST["newsid"];
			$priv_res = mysql_query($priv_query);

			if (isset($_REQUEST["privs"]) && is_array($_REQUEST["privs"]))
			{
				foreach ($_REQUEST["privs"] as $priv)
				{
					$ins_query = "Insert into tblpriviledgeuser_admin (PUUser, PUPriviledge) values (".$_REQUEST["newsid"].",".$priv.")";
					$ins_res =mysql_query($ins_query);
				}
			}
		}
	}
	else
	{
		$info_text .= "Chyba během ukládání uživatele. Nebyly zadány všechny povinné parametry";
		$alertype = " alert-danger ";
	}
}
//posila novou
else
{

	if (@$_REQUEST["send"])
	{

		if(strlen(trim(@$_POST["user_login"])))
		{

			$query = "Insert into ".$table_name;

			$col_query =" ULogin";
			$val_query ="  '".trim(strip_tags($_POST["user_login"]))."'";

			$col_query .=", UName";
			$val_query .=" , '".trim(strip_tags($_POST["user_name"]))."'";

			$col_query .=", USurname";
			$val_query .=" , '".trim($_POST["user_surname"])."'";

			$col_query .=", UHis";
			$val_query .=" , '".trim($_POST["UHis"])."'";

			$col_query .=", UType";
			$val_query .=",  0";

             if (isset($image_filename) && strlen($image_filename)>0)
            {
                $col_query .= ", UFoto";
                $val_query .= ", '".$image_filename."'";
            }

			if (strlen($_POST["user_pass"]))
			{
				if ($_POST["user_pass"] == $_POST["user_pass_repeat"])
				{
					$col_query .=", UPass";
					$val_query .=",  PASSWORD('".trim($_POST["user_pass"])."')";
				}
			}


			if (isset($_POST["can_login"]))
			{
				$col_query .=", UCanLogin";
				$val_query .=",  1";
			}
			else
			{
				$col_query .=", UCanLogin";
				$val_query .=" , 0";
			}

			  $query .= " (".$col_query.") values (".$val_query.")";

			$res = mysql_query($query);

			if ($res && @mysql_affected_rows($link)>0)
			{
				$info_text .= "Uživatel úspěšně uložen.";
				$alertype = " alert-success ";

				
				$new_user_id = @mysql_insert_id($link);

				if (isset($_REQUEST["privs"]) && is_array($_REQUEST["privs"]) && $new_user_id > 0)
				{
					foreach ($_REQUEST["privs"] as $priv)
					{
						$ins_query = "Insert into tblpriviledgeuser (PUUser, PUPriviledge) values (".$new_user_id.",".$priv.")";
						$ins_res =  mysql_query($ins_query);
					}
				}
			}
			else
			{
				$info_text .= "Uživatel nebyl uložen.";
				$alertype = " alert-danger ";
			}
		}
		else
		{
			$info_text .= "Chyba během ukládání uživatele. Nebyly zadány všechny povinné parametry!";
			$alertype = " alert-danger ";
		}
	}

}
// ende
if(@$info_text!="")
{
    ?>
	<div class="alert <?php echo $alertype; ?>" role="alert"><?php echo $info_text; ?></div>
	<?php
}




$time=time();

$result = mysql_query("SELECT * FROM $table_name where Deleted = 0 ORDER BY USurname,UName,ULogin");
?>
 <div class="table-responsive">
 <table  id='tableOUT' class="table table-striped table-bordered m-b-0 toggle-circle footable-loaded footable tablet breakpoint  "> 
<thead>  
	<tr>
    <th class='short center'>Smazat</th>
    <th>Hodnotitel</th>
    <th class='short center'>Hodnotí</th>

</tr>
</thead>
<tbody>
<?php
if ( mysql_num_rows($result)>0)
{
	while ($resarr =  mysql_fetch_array($result))
	{
		$newsid = $resarr["ID"];
		$text = $resarr["USurname"].", ".$resarr["UName"]." - ".$resarr["ULogin"];

?>
 
 <tr><td class='short center'>
 
 <a onClick="return confirm('Skutečně chcete <?php echo $jeho; ?> vymazat z databáze?')" href="index.php?id=<?php echo $page_name; ?>&amp;delete=<?php echo $newsid; ?>"  class="far fa-times-circle text-danger" ></a>

 
	
  </td><td>

 <a href="index.php?id=<?php echo $page_name; ?>new&amp;newsid=<?php echo $newsid; ?>"><strong><?php echo strip_tags($text)," "; ?></strong></a>
 
  </td><td class='short center'>

 
 

 <?php if($resarr["UCanLogin"] == 1) { ?>
  <a onClick="return confirm('Skutečně chcete vyřadit hodnotitele? Má uvolněná všechna hodnocení?')"  href="index.php?id=<?php echo $page_name; ?>&amp;invalid=<?php echo $newsid; ?>" class="far fa-check-circle text-success"   data-toggle='tooltip' data-placement='left'  title="Uživatel hodnotí (klik = Zakázat)" ></a>
  <?php  }  else {  ?>
  <a href="index.php?id=<?php echo $page_name; ?>&amp;valid=<?php echo $newsid; ?>" class="far fa-times-circle text-danger"   data-toggle='tooltip' data-placement='left'  title="Uživatel nehodnotí (klik = Povolit)" ></a>
<?php } ?>
 
<?php 
	}
	?>
    </td></tr>
	<?php
    }
?>
</tbody>

  </table>
</div>
 <a href="index.php?id=<?php echo $page_name; ?>new" class="fas fa-plus-circle"><span >Přidat další</span></a> 
 
 <script type="text/javascript">
      $(document).ready( function () {
        $('#tableOUT').DataTable( {
        paging: false ,
        "order": [[ 1,  "asc" ]]
        } );
      } );
 
  </script>
 