<h1>Nastavení otázek kategorie Diverzita</h1>

<?php
        $warningTXT = "V systému je nastaveno, že soutěž je ve fáz testování, všechny změny jsou povoleny.";
        $warningstyle = "success";
if(Globals::$GLOBAL_LOCK_BY_CONTEST == 1) { 
        $contDisable = 'disabled'; 
        $warningTXT = "V systému je nastaveno, že probíhá <strong>vyplňování soutěžních přihlášek</strong>. Proto už není možné provádět některé změny v jejich nastavení.";
        $warningstyle = "danger";
}
if(Globals::$GLOBAL_LOCK_BY_JURY == 1) { 
        $juryDisable = 'disabled'; 
        $warningTXT2  = "<br/>V systému je nastaveno, že probíhá <strong>hodnocení soutěžních přihlášek</strong>. Proto už není možné provádět některé změny v jejich nastavení.";
        $warningstyle = "warning";
}

if(strlen($warningTXT) > 0) {
      ?>
        <div class="alert alert-<?php echo $warningstyle; ?>" role="alert">
        <?php
                echo $warningTXT.$warningTXT2;
        ?>
        </div>
      <?php  
}

?>





<style>
.nr{
        width: 30px;
}
.nrlong {
        width: 45px;  
}
table { font-size: 10px }
table#tableOUT td, table#tableOUT th { font-size: 11px!important }
</style>


<?php

$tablename = "tbl_app_settings_projects5";
if(strlen($_POST['mainsubmit']) > 0) {

       $info_text .= "Neukládám žádné změny";
       $alertype = " alert-secondary ";

       if (is_array($_POST['hiddenID'])) {
            
       foreach ($_POST['hiddenID'] as $sid) {
             
               $upquery = "Update ".$tablename;
               $upquery .= " SET  settingsMOFnumber = ".$_POST['settingsMOFnumber'][$sid];
               //$upquery .= "  ,    settingsVOFnumber = ".$_POST['settingsVOFnumber'][$sid];
               //$upquery .= "  ,  settingsMOFnumberOLD = ".$_POST['settingsMOFnumberOLD'][$sid];
               //$upquery .= "  ,  settingsVOFnumberOLD = ".$_POST['settingsVOFnumberOLD'][$sid];
               //$upquery .= "  ,  settingsVOFADVnumberOLD = ".$_POST['settingsVOFADVnumberOLD'][$sid];
               $upquery .= "  ,  settingsTxt = '".$_POST['settingsTxt'][$sid]."' "; 
               if(isset($_POST['settingsType'][$sid])) { $upquery .= "  ,  settingsType = '".$_POST['settingsType'][$sid]."' ";}
               $upquery .= "  ,  settingsKolikOptions = '".$_POST['settingsKolikOptions'][$sid]."' ";
               $upquery .= "  ,  settingsKolik = '".$_POST['settingsKolik'][$sid]."' ";
               $upquery .= "  ,  settingsOthers = '".$_POST['settingsOthers'][$sid]."' ";
               $upquery .= "  ,  settingsLimit = '".$_POST['settingsLimit'][$sid]."' ";
               $upquery .= "  ,  settingsFree = '".$_POST['settingsFree'][$sid]."' ";
               //$upquery .= "  ,  settingsNext = ".$_POST['settingsNext'][$sid];
               //$upquery .= "  ,  settingsKat = ".$_POST['settingsKat'][$sid];    
               $upquery .= "  ,  lastYearID = '".$_POST['lastYearID'][$sid]."' ";
               $upquery .= "  ,  settingsHelp =  '".$_POST['settingsHelp'][$sid]."' ";
               $upquery .= "  ,  settingsHelp2 =  '".$_POST['settingsHelp2'][$sid]."' ";    
               $upquery .= "  ,  juryAuto =  '".$_POST['juryAuto'][$sid]."' ";
               $upquery .= "  ,  juryPointsWeght  =  '".$_POST['juryPointsWeght'][$sid]."' ";
               $upquery .= "  ,  settingsMOFnumberOLD  =  '".$_POST['settingsMOFnumberOLD'][$sid]."' ";
               $upquery .= "  ,  juryClaim  =  '".$_POST['juryClaim'][$sid]."' ";
               $upquery .= "  ,  juryMerge  =  '".$_POST['juryMerge'][$sid]."' ";
                $upquery .= "  where ID = ".$sid;
               mysql_query($upquery);
               if (mysql_affected_rows($link) > 0)
               {
                       $info_text = "Změny uloženy";
                       $alertype = " alert-primary ";
               }
           }
     }
}


if(@$info_text!="")
{
    ?>
	<div class="alert <?php echo $alertype; ?>" role="alert"><?php echo $info_text; ?></div>
	<?php
}
?>


<form action='index.php?id=settings-TOP5' method="post" name="noname" id="cnt2"  enctype="multipart/form-data">

<table  id='tableOUT' class="table table-striped table-bordered m-b-0 toggle-circle footable-loaded footable tablet breakpoint"> 
<tr>
<th>NR.</th>
<th>Text otázky</th>
<th><a href='/admin/index.php?id=pages&type=helps'>Help </a> 
<!--<th>Napojení</th>
<th>Kategorie</th>-->
<th>Typ</th>
<th>Limit znaků</th>
<th>Počet možností<br/>jejich výpis (odděl;)</th>
<th>Dopň. info?</th>
<th>Povinné?</th>
<th>ID&nbsp;2018</th>
<th>Kdo hodnotí? (max?)</th>
<th>Hodn. spojení</th>
 
</tr>

 
<?php

$query =  
"Select *, ".$tablename.".ID as SID, tbl_app_settings_Types.ID as TID, tbl_app_settings_Category.ID as CID, tbl_app_settings_Next.ID as XID From ".$tablename."  
 LEFT JOIN tbl_app_settings_Types ON ".$tablename.".settingsType = tbl_app_settings_Types.ID 
 LEFT JOIN tbl_app_settings_Next ON ".$tablename.".settingsNext = tbl_app_settings_Next.ID 
 LEFT JOIN tbl_app_settings_Category ON ".$tablename.".settingsKat = tbl_app_settings_Category.ID 
 Order by ".$tablename.".settingsOrder";
$res =    mysql_query($query);
if ($res && mysql_num_rows($res)>0)
{
while ($row = @mysql_fetch_array($res)){ 
   echo "<tr>";
 
   echo "<td><input type='number'  class='nr' title='Číslo MOF' name='settingsMOFnumber[".$row['SID']."]' value='".$row['settingsMOFnumber']."' title='Číslo MOF a ID v DB je ".$row['SID']."' /></td>";
   echo "<td><input name='settingsTxt[".$row['SID']."]' value='".$row['settingsTxt']."' class='longtext'  data-toggle='tooltip' data-placement='top'  title='Text otázky | ".$row['settingsTxt']."' /></td>";   
  
  
   echo "<td><select name='settingsHelp[".$row['SID']."]' class='text' title='Help pro soutěžící'>";
   $typeQuery = "Select *  From tbl_helps Where pagesPublic = 1 ";
   $tres =    mysql_query($typeQuery);
    if ($tres && mysql_num_rows($tres)>0)
    {   
    echo "<option value='0'>-- nevybráno --</option>";
    while ($trow = @mysql_fetch_array($tres)){ 
            if($trow['ID'] == $row['settingsHelp'])  { $tselect = ' selected ';  }else {  $tselect = ' '; }

            echo "<option ".$tselect." value='".$trow['ID']."'>".$trow['pagesNadpis']."</option>";
    }
    }
    echo "</select> ";   
  
 
      echo "<td><select ".$contDisable." name='settingsType[".$row['SID']."]' class='text'  title='Typ otázky'>";
      $typeQuery = "Select * From tbl_app_settings_Types";
      $tres =    mysql_query($typeQuery);
       if ($tres && mysql_num_rows($tres)>0)
       {   
       echo "<option value='0'>-- nevybráno --</option>";
       while ($trow = @mysql_fetch_array($tres)){ 
               if($trow['ID'] == $row['TID'])  { $tselect = ' selected ';  }else {  $tselect = ' '; }
               $actualType = $row['TID'];
               echo "<option ".$tselect." value='".$trow['ID']."'>".$trow['typeTxt']."</option>";
       }
       }
       echo "</select></td>";
 
  $volby = array(4,5,7);
  if (in_array($actualType, $volby)) { $schovme = '';   }else { $schovme = 'style="display:none"'; }
  $texty = array(3,1000);
  if (in_array($actualType, $texty)) { $schovme2 = '';   }else { $schovme2 = 'style="display:none"'; }

  echo "<td><input  type='number'  data-toggle='tooltip' data-placement='top'  class='nrlong' ".$schovme2." title='Limit počtu znaků \r(0 = neomezeno)'  name='settingsLimit[".$row['SID']."]' value='".$row['settingsLimit']."' />";    
   echo "<td><input ".$contDisable." type='number'  class='nr' ".$schovme." title='Počet možností '  name='settingsKolik[".$row['SID']."]' value='".$row['settingsKolik']."' />";    
   echo " <input ".$contDisable." ".$schovme." title='Výčet možností oddělených středníkem' name='settingsKolikOptions[".$row['SID']."]' value='".$row['settingsKolikOptions']."' class='ltext' /></td>";   
   
   if( $row['settingsOthers'] == 1)  { $oselect1 = ' selected ';  $oselect0 = ''; }else { $oselect0 = '  selected  '; $oselect1 = '';  }
   echo "<td><select ".$schovme." name='settingsOthers[".$row['SID']."]' ><option value='0' ".$oselect0." >Doplň. NE</option><option value='1' ".$oselect1.">Doplň. ANO</option></select></td>";

   if( $row['settingsFree'] == 1)  { $fselect1 = ' selected ';  $fselect0 = ''; }else { $fselect0 = '  selected  '; $fselect1 = '';  }
   echo "<td><select  name='settingsFree[".$row['SID']."]' ><option value='0' ".$fselect0." >Povinné</option><option value='1' ".$fselect1.">Free</option></select></td>";
  
  
  // echo "<input title='Staré MOF ID'  name='settingsMOFnumberOLD' value='".$row['settingsMOFnumberOLD']."'  />";   
 //  echo "<input title='Staré VOF ID'  name='settingsVOFnumberOLD' value='".$row['settingsVOFnumberOLD']."'  />";   
  // echo "<input title='Staré VOF Advnced ID'  name='settingsVOFADVnumberOLD' value='".$row['settingsVOFADVnumberOLD']."'  />";  
   echo "<td><input type='number'  class='nr' title='Staré ID MYSQL | Staré Webové číslo   ".$row['settingsMOFnumberOLD'].", VOF ".$row['settingsVOFnumberOLD'].", VOFADV ".$row['settingsVOFADVnumberOLD']."  a v DB je to ".$row['SID']."'  name='lastYearID[".$row['SID']."]' value='".$row['lastYearID']."'  />";  
  
   
   if( $row['juryAuto'] == 1)  { $fselect1 = ' selected ';  $fselect0 = '';    $fselect3 = '';  $fselect4 = '';}
   elseif( $row['juryAuto'] == 0){ $fselect0 = '  selected  '; $fselect1 = '';   $fselect3 = '';  $fselect4 = '';}
   elseif( $row['juryAuto'] == 4){ $fselect0 = '  selected  '; $fselect1 = '';   $fselect3 = '';  $fselect4 = ' selected ';}
   else { $fselect0 = '     '; $fselect1 = '';    $fselect3 = ' selected '; $fselect4 = ''; }
   echo "<td><select class='juryAuto'  ".$juryDisable."  name='juryAuto[".$row['SID']."]' ><option value='0' ".$fselect0." >MAN</option><option value='1' ".$fselect1.">AUT</option><option value='3' ".$fselect3.">NE</option></select>";
   echo "<input type='number'   ".$juryDisable."  class='nr' title='U otázek, které hodnotí lidé, vypsat MAX hodnotu za top hodnocení. U automatu vypsat body pro jednotlivé možnosti, jak jsou uvedeny v možnostech. Oddělit středníky' name='juryPointsWeght[".$row['SID']."]' value='".$row['juryPointsWeght']."' /></td>";   

   if( $row['juryMerge'] == 1)  { $mselect1 = ' selected ';  $mselect0 = '';    $mselect3 = '';  $mselect4 = '';}
   else{ $mselect0 = '  selected  '; $mselect1 = '';   $mselect3 = '';  $mselect4 = '';}
   

   echo "<td><select class='juryMerge' ".$juryDisable." name='juryMerge[".$row['SID']."]' ><option value='0' ".$mselect0." >NE</option>
   <option value='1' ".$mselect1.">ANO</option></select></td>";
 
   echo "<input type='hidden' name='hiddenID[".$row['SID']."]' value='".$row['SID']."'/></td>";

   if($row['settingsVOFnumber'] > 0) {
        $bodiki = $row['juryPointsWeght'];
        $bodiki = explode(';',$row['juryPointsWeght']);
        rsort($bodiki);
        $maxVOFbodiki = $maxVOFbodiki + $bodiki[0];

   }
 
   if($row['settingsMOFnumber'] > 0) {
        $bodiki2 = $row['juryPointsWeght'];
        $bodiki2 = explode(';',$row['juryPointsWeght']);
        rsort($bodiki2);
        $maxMOFbodiki = $maxMOFbodiki + $bodiki2[0];

   }   

   echo "</tr> ";




}
}
echo "</table>"
//echo "<p class='MXV'>Maxmální počet bodů VOF = ".$maxVOFbodiki."</p>";
//echo "<p class='MXV'>Maxmální počet bodů MOF = ".$maxMOFbodiki."</p>";
?>
  <div class='sender'>      
  <input type="submit" class="btn btn-primary "  id="contactsubmit" value="Uložit otázky" name="mainsubmit"  > 
  </div>
  </form>