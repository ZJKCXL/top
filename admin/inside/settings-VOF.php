<h1>Nastavení otázek VOF|MOF</h1>

<?php
        $warningTXT = "V systému je nastaveno, že soutěž je ve fázi testování, všechny změny jsou povoleny.";
        $warningstyle = "success";
if(Globals::$GLOBAL_LOCK_BY_CONTEST == 1) { 
        $contDisable = 'disabled'; 
        $warningTXT = "V systému je nastaveno, že probíhá <strong>vyplňování soutěžních přihlášek</strong>. Proto už není možné provádět některé změny v jejich nastavení.";
        $warningstyle = "danger";
}
if(Globals::$GLOBAL_LOCK_BY_JURY == 1) { 
        $juryDisable = 'disabled'; 
        $warningTXT2  = "<br/>V systému je nastaveno, že probíhá <strong>hodnocení soutěžních přihlášek</strong>. Proto už není možné provádět některé změny v jejich nastavení.";
        $warningstyle = "danger";
}

if(strlen($warningTXT) > 0) {
      ?>
        <div class="alert alert-<?php echo $warningstyle; ?>" role="alert">
        <?php
                echo $warningTXT.$warningTXT2;
        ?>
        </div>
      <?php  
}

?>


<style>
.nr{
        width: 40px;
}
.invisibl {
        color: #e7e7e7;
}
table { font-size: 10px }
#contactsubmitFloat { position: fixed;
    top: 70px;
    right: 25px;
    z-index: 1000; }
</style>
 
<?php
//  var_dump($_POST[settingsVOF]);
  $tablename = "tbl_app_settings";

  if(strlen($_POST['mainsubmit']) > 0) {

        $info_text .= "Neukládám žádné změny";
        $alertype = " alert-secondary ";
 
        if (is_array($_POST['hiddenID'])) {
             
        foreach ($_POST['hiddenID'] as $sid) {
  
    $upquery = "Update ".$tablename;
    $upquery .= " SET  settingsMOFnumber = ".$_POST['settingsMOFnumber'][$sid];
    $upquery .= "  ,    settingsVOFnumber = ".$_POST['settingsVOFnumber'][$sid];
    //$upquery .= "  ,  settingsMOFnumberOLD = ".$_POST['settingsMOFnumberOLD'][$sid];
    //$upquery .= "  ,  settingsVOFnumberOLD = ".$_POST['settingsVOFnumberOLD'][$sid];
    //$upquery .= "  ,  settingsVOFADVnumberOLD = ".$_POST['settingsVOFADVnumberOLD'][$sid];
    $upquery .= "  ,  settingsTxt = '".$_POST['settingsTxt'][$sid]."' ";
    if(isset($_POST['settingsType'][$sid])) { $upquery .= "  ,  settingsType = '".$_POST['settingsType'][$sid]."' ";}
    if(isset($_POST['settingsKolikOptions'][$sid])) { $upquery .= "  ,  settingsKolikOptions = '".$_POST['settingsKolikOptions'][$sid]."' ";}
    if(isset($_POST['settingsKolik'][$sid])) { $upquery .= "  ,  settingsKolik = '".$_POST['settingsKolik'][$sid]."' ";}    
    if($_POST[settingsREP][$sid] == '1') {
    $upquery .= "  ,  settingsREP = 1 ";
    }
    else{
    $upquery .= "  ,  settingsREP = 0 ";
    }

    if($_POST[settingsVOF][$sid] == '1') {
        $upquery .= "  ,  settingsVOF = 1 ";
        }
        else{
        $upquery .= "  ,  settingsVOF = 0 ";
    }

   if($_POST[settingsMOF][$sid] == '1') {
                $upquery .= "  ,  settingsMOF = 1 ";
                }
                else{
                $upquery .= "  ,  settingsMOF = 0 ";
    }            
    $upquery .= "  ,  settingsOthers = ".$_POST['settingsOthers'][$sid];
    $upquery .= "  ,  settingsFree = ".$_POST['settingsFree'][$sid];
    $upquery .= "  ,  settingsNext = ".$_POST['settingsNext'][$sid];
    $upquery .= "  ,  settingsKat = ".$_POST['settingsKat'][$sid];    
    $upquery .= "  ,  lastYearID = ".$_POST['lastYearID'][$sid];
    $upquery .= "  ,  settingsHelp =  '".$_POST['settingsHelp'][$sid]."' ";

    if(isset($_POST['juryAuto'][$sid])) { $upquery .= "  ,  juryAuto = '".$_POST['juryAuto'][$sid]."' ";}  
    if(isset($_POST['juryPointsWeght'][$sid])) { $upquery .= "  ,  juryPointsWeght = '".$_POST['juryPointsWeght'][$sid]."' ";}  
    if(isset($_POST['juryMerge'][$sid])) { $upquery .= "  ,  juryMerge = '".$_POST['juryMerge'][$sid]."' ";}  

    $upquery .= "  ,  settingsMOFnumberOLD  =  '".$_POST['settingsMOFnumberOLD'][$sid]."' ";


   $upquery .= "  where ID = ".$sid;

 //  echo "<hr/>". $upquery;

   mysql_query($upquery);

    if (mysql_affected_rows($link) > 0)
    {
            $info_text = "Změny uloženy";
            $alertype = " alert-success ";
    }
}
}
}


if(@$info_text!="")
{
    ?>
	<div class="alert <?php echo $alertype; ?>" role="alert"><?php echo $info_text; ?></div>
	<?php
}
?>


<form action='index.php?id=settings-VOF' method="post" name="noname" id="cnt2"  enctype="multipart/form-data">

<table  id='tableOUT' class="table table-striped table-bordered m-b-0 toggle-circle footable-loaded footable tablet breakpoint"> 
<tr>
<th>NR.MOF</th>
<th>NR.VOF</th>
<th>MOF|VOF|REP</th>
<th>Text otázky</th>
<th><a href='/admin/index.php?id=pages&type=helps'>Help </a> 
<th>Napojení</th>
<th>Kategorie</th>
<th>Typ</th>
<th>Počet možností<br/>jejich výpis (odděl;)</th>
<th>Dopň. info?</th>
<th>Povinné?</th>
<th>ID&nbsp;2018</th>
<th>Kdo hodnotí? (max?)</th>
<th>SLoučit v Hodn.</th>
</tr>



<?php
    $query =  
"Select *, ".$tablename.".ID as SID, tbl_app_settings_Types.ID as TID, tbl_app_settings_Category.ID as CID, tbl_app_settings_Next.ID as XID From ".$tablename."  
 LEFT JOIN tbl_app_settings_Types ON ".$tablename.".settingsType = tbl_app_settings_Types.ID 
 LEFT JOIN tbl_app_settings_Next ON ".$tablename.".settingsNext = tbl_app_settings_Next.ID 
 LEFT JOIN tbl_app_settings_Category ON ".$tablename.".settingsKat = tbl_app_settings_Category.ID 
 Order by ".$tablename.".settingsOrder";
$res =    mysql_query($query);
if ($res && mysql_num_rows($res)>0)
{
while ($row = @mysql_fetch_array($res)){ 

   if($row['settingsREP'] == 1) { $checkREP = 'checked'; } else {  $checkREP = '';  }
   if($row['settingsMOF'] == 1) { $checkMOF = 'checked'; } else {  $checkMOF = '';  }
   if($row['settingsVOF'] == 1) { $checkVOF = 'checked'; } else {  $checkVOF = '';  }
   if($row['settingsMOFnumber'] < 1) { $mofnr = 'invisibl'; }else { $mofnr = '';  }
   if($row['settingsVOFnumber'] < 1) { $vofnr = 'invisibl'; }else { $vofnr = '';  }
   $indour = $row['SID'];
   echo "<tr>";
 
   echo "<td><input class='nr ".$mofnr."' type='number' title='Číslo MOF' name='settingsMOFnumber[".$indour."]' value='".$row['settingsMOFnumber']."' title='Číslo MOF a ID v DB je ".$row['SID']."' /></td>";
   echo "<td><input class='nr ".$vofnr."' type='number'  title='Číslo VOF' name='settingsVOFnumber[".$indour."]' value='".$row['settingsVOFnumber']."' title='Číslo VOF' /></td>";   
   echo "<td>
   <input  data-toggle='tooltip' data-placement='top' type='checkbox' ".$checkMOF." name='settingsMOF[".$indour."]' value='1'  title='Zaškrtnuté zobrazit mezi MOF'  />
   <input  data-toggle='tooltip' data-placement='top' type='checkbox' ".$checkVOF." name='settingsVOF[".$indour."]' value='1'  title='Zaškrtnuté zobrazit mezi VOF'  />

   <input  data-toggle='tooltip' data-placement='top' type='checkbox' ".$checkREP." name='settingsREP[".$indour."]' value='1'  title='Zaškrtnuté zobrazit v reportingu'  />
   </td>";    
   echo "<td><input data-toggle='tooltip' data-placement='top'  name='settingsTxt[".$indour."]' value='".$row['settingsTxt']."' class='longtext' title='Text otázky : ".$row['settingsTxt']."' /></td>";   
 
   echo "<td><select name='settingsHelp[".$indour."]' class='text' title='Help pro soutěžící'>";
   $typeQuery = "Select *  From tbl_helps Where pagesPublic = 1 ";
   $tres =    mysql_query($typeQuery);
    if ($tres && mysql_num_rows($tres)>0)
    {   
    echo "<option value='0'>-- nevybráno --</option>";
    while ($trow = @mysql_fetch_array($tres)){ 
            if($trow['ID'] == $row['settingsHelp'])  { $tselect = ' selected ';  }else {  $tselect = ' '; }

            echo "<option ".$tselect." value='".$trow['ID']."'>".$trow['pagesNadpis']."</option>";
    }
    }
    echo "</select></td>";   
  


    echo "<td><select name='settingsNext[".$indour."]' class='text' title='Spojení do skupiny'>";
    $typeQuery = "Select * From tbl_app_settings_Next";
    $tres =    mysql_query($typeQuery);
     if ($tres && mysql_num_rows($tres)>0)
     {   
     echo "<option value='0'>-- nevybráno --</option>";
     while ($trow = @mysql_fetch_array($tres)){ 
             if($trow['ID'] == $row['XID'])  { $tselect = ' selected ';  }else {  $tselect = ' '; }
 
             echo "<option ".$tselect." value='".$trow['ID']."'>".$trow['nextName']."</option>";
     }
     }
     echo "</select></td>";   
     
     echo "<td><select name='settingsKat[".$indour."]' class='text' title='Kategorie otázky'>";
     $typeQuery = "Select * From tbl_app_settings_Category";
     $tres =    mysql_query($typeQuery);
      if ($tres && mysql_num_rows($tres)>0)
      {   
      echo "<option value='0'>-- nevybráno --</option>";
      while ($trow = @mysql_fetch_array($tres)){ 
              if($trow['ID'] == $row['CID'])  { $tselect = ' selected ';  }else {  $tselect = ' '; }
  
              echo "<option ".$tselect." value='".$trow['ID']."'>".$trow['catName']."</option>";
      }
      }
      echo "</select></td>";      


      echo "<td><select ".$contDisable." name='settingsType[".$indour."]' class='text'  title='Typ otázky'> ";
      $typeQuery = "Select * From tbl_app_settings_Types";
      $tres =    mysql_query($typeQuery);
       if ($tres && mysql_num_rows($tres)>0)
       {   
       echo "<option value='0'>-- nevybráno --</option>";
       while ($trow = @mysql_fetch_array($tres)){ 
               if($trow['ID'] == $row['TID'])  { $tselect = ' selected ';  }else {  $tselect = ' '; }
   
               echo "<option ".$tselect." value='".$trow['ID']."'>".$trow['typeTxt']."</option>";
       }
       }
       echo "</select></td>";

 
  if($row['TID'] == 1) { $schovme = 'style="display:none"';  }else { $schovme = '"'; }
   echo "<td><input ".$contDisable." class='nr' type='number'  ".$schovme." title='Počet možností '  name='settingsKolik[".$indour."]' value='".$row['settingsKolik']."' />";    
   echo "<input ".$contDisable." data-toggle='tooltip' data-placement='top'  ".$schovme." title='Výčet možností oddělených středníkem' name='settingsKolikOptions[".$indour."]' value='".$row['settingsKolikOptions']."' class='ltext' /></td>";   
   
   if( $row['settingsOthers'] == 1)  { $oselect1 = ' selected ';  $oselect0 = ''; }else { $oselect0 = '  selected  '; $oselect1 = '';  }
   echo "<td><select ".$schovme." name='settingsOthers[".$indour."]' ><option value='0' ".$oselect0." >Doplň. NE</option><option value='1' ".$oselect1.">Doplň. ANO</option></select></td>";

   if( $row['settingsFree'] == 1)  { $fselect1 = ' selected ';  $fselect0 = ''; }else { $fselect0 = '  selected  '; $fselect1 = '';  }
   echo "<td><select ".$schovme." name='settingsFree[".$indour."]' ><option value='0' ".$fselect0." >Povinné</option><option value='1' ".$fselect1.">Free</option></select></td>";
  
  
  // echo "<input title='Staré MOF ID'  name='settingsMOFnumberOLD' value='".$row['settingsMOFnumberOLD']."'  />";   
 //  echo "<input title='Staré VOF ID'  name='settingsVOFnumberOLD' value='".$row['settingsVOFnumberOLD']."'  />";   
  // echo "<input title='Staré VOF Advnced ID'  name='settingsVOFADVnumberOLD' value='".$row['settingsVOFADVnumberOLD']."'  />";  
   echo  "<td> <input class='nr' type='number'  title='Staré ID MYSQL | Staré Webové číslo MOF ".$row['settingsMOFnumberOLD'].", VOF ".$row['settingsVOFnumberOLD'].", VOFADV ".$row['settingsVOFADVnumberOLD']."  a v DB je to ".$indour."'  name='lastYearID[".$indour."]' value='".$row['lastYearID']."'  />";  
  
  // echo " <input class='nr' type='number'  title='Staré ID MYSQL | Staré Webové číslo MOF ".$row['settingsMOFnumberOLD'].", VOF ".$row['settingsVOFnumberOLD'].", VOFADV ".$row['settingsVOFADVnumberOLD']."  a v DB je to ".$indour."'  name='settingsMOFnumberOLD[".$indour."]' value='".$row['settingsMOFnumberOLD']."'  /></td>";  

   if( $row['juryAuto'] == 1)  { $fselect1 = ' selected ';  $fselect0 = '';    $fselect3 = '';  $fselect4 = '';}
   elseif( $row['juryAuto'] == 0){ $fselect0 = '  selected  '; $fselect1 = '';   $fselect3 = '';  $fselect4 = '';}
   elseif( $row['juryAuto'] == 4){ $fselect0 = '  selected  '; $fselect1 = '';   $fselect3 = '';  $fselect4 = ' selected ';}
   else { $fselect0 = '     '; $fselect1 = '';    $fselect3 = ' selected '; $fselect4 = ''; }
   echo "<td><select class='juryAuto' ".$juryDisable."  name='juryAuto[".$indour."]' ><option value='0' ".$fselect0." >MAN</option><option value='1' ".$fselect1.">AUT</option><option value='3' ".$fselect3.">NE</option></select>";
   echo " <input width='50px'".$juryDisable."   data-toggle='tooltip' data-placement='top'  title='U otázek, které hodnotí lidé, vypsat MAX hodnotu za top hodnocení. U automatu vypsat bodz pro jednotlivé možnosti, jak jsou uvedeny v možnostech. Oddělit středníky' name='juryPointsWeght[".$indour."]' value='".$row['juryPointsWeght']."'  /></td>";   

   if( $row['juryMerge'] == 1)  { $mselect1 = ' selected ';  $mselect0 = '';                $mselect3 = '';          $mselect4 = '';        }
   elseif( $row['juryMerge'] == 3)  { $mselect1 = '  ';          $mselect0 = '';                $mselect3 = 'selected';  $mselect4 = '';        }
   elseif( $row['juryMerge'] == 4)  { $mselect1 = '  ';          $mselect0 = '';                $mselect3 = '';          $mselect4 = 'selected';}
   else{                          $mselect1 = '';             $mselect0 = ' selected  ';    $mselect3 = '';          $mselect4 = '';        }
   

   echo "<td>
   <select class='juryMerge' ".$juryDisable."  name='juryMerge[".$indour."]' >
   <option value='0' ".$mselect0." >Neslučovat </option>
   <option value='3' ".$mselect3.">Sloučit <</option>
   <option value='1' ".$mselect1.">Sloučit <></option>
   <option value='4' ".$mselect4.">Sloučit ></option>
   </select></td>";
  // echo "<input  name='juryClaim' title='Text, který se vypíše hodnotitelům' value='".$row['juryClaim']."' style='width: 128px' />";   

  echo "<input type='hidden' name='hiddenID[".$row['SID']."]' value='".$row['SID']."'/> ";
 
   if($row['settingsVOFnumber'] > 0) {
        $bodiki = $row['juryPointsWeght'];
        $bodiki = explode(';',$row['juryPointsWeght']);
        rsort($bodiki);
        $maxVOFbodiki = $maxVOFbodiki + $bodiki[0];

   }
 
   if($row['settingsMOFnumber'] > 0) {
        $bodiki2 = $row['juryPointsWeght'];
        $bodiki2 = explode(';',$row['juryPointsWeght']);
        rsort($bodiki2);
        $maxMOFbodiki = $maxMOFbodiki + $bodiki2[0];

   }   

 

   echo "</tr>";


}
}
//echo "<p class='MXV'>Maxmální počet bodů VOF = ".$maxVOFbodiki."</p>";
//echo "<p class='MXV'>Maxmální počet bodů MOF = ".$maxMOFbodiki."</p>";
?>
 </table>
 <div class='sender'>      
  <input type="submit" class="btn btn-primary "  id="contactsubmit" value="Uložit otázky" name="mainsubmit"  > 
  </div>
  <input type="submit" class="btn btn-primary "  id="contactsubmitFloat" value="Uložit otázky" name="mainsubmit"  > 
 </form> 