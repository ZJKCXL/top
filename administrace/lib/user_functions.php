<?php

function feistel_encrypt($in_text)
{
	$j=0;
	$result = "";
	for ($i=0;$i<strlen($in_text);$i++)
	{
		$in_byte = ord(substr($in_text,$i,1));
		$elem = pow(-1,$i%2);
		$data = $in_byte + $elem;
		$result .= chr($data);
	}
	$result = base64_encode($result);
	return $result;
	exit;
}

function feistel_decrypt($in_text)
{
	$j=0;
	$result = "";
	$in_text = base64_decode($in_text);
	for ($i=0;$i<strlen($in_text);$i++)
	{
		$in_byte = ord(substr($in_text,$i,1));
		$elem = pow(-1,$i%2);
		$data = $in_byte - $elem;
		$result .= chr($data);
	}
	return $result;
	exit;
}

function user_logged_in()
{                  
	 global $admincookie;
 
 $in_admincookie =   $_COOKIE[$admincookie["name"]];

	$result = false;

	if (strlen($in_admincookie)>0)
	{
		$decoded_admincookie = feistel_decrypt($in_admincookie);
		$exploded_admincookie = explode(";",$decoded_admincookie);
		$result = ($exploded_admincookie[0] == md5($_SERVER['HTTP_USER_AGENT']));

	}

	return $result;
}



function user_login($user_name, $user_pass)
{
 
	$result = false;
	global $conn;
	global $admincookie;
	$link= @mysql_connect($conn["host"],$conn["user"],$conn["pass"]);
	if (!$link)
	{
  	$link = @mysql_connect($conn["host"],$conn["user2"],$conn["pass2"]);
	}
	if (!$link)
	{
		echo "Connection pool full";
		return false;
	}

  $db=mysql_select_db($conn["db"], $link);
 
	if (!$db)
	{
		return false;
	}

  $query = "Select tbluser.* from tbluser where ULogin = '".$user_name."' and UPass =PASSWORD('".$user_pass."') and UCanLogin = 1 and Valid = 1 and Deleted = 0";

	$res = @mysql_query($query);
	if ($res && @mysql_num_rows($res)>0)
	{
		$resarr = @mysql_fetch_array($res);
		$agent =$_SERVER['HTTP_USER_AGENT'];
		$agent = md5($agent);


	   $admincookie_data = $agent.";".$resarr["ID"].";".$resarr["UType"].";".$resarr["UFirm"];
		 	$admincookie_data = feistel_encrypt($admincookie_data);
		if (strlen($admincookie_data)>0)
		{
			 	$result = @setcookie($admincookie["name"],$admincookie_data,null,$admincookie["path"],$admincookie["domain"]);
		}
	}
	return $result;
}

function user_logout()
{
 
	$result = false;

	global $admincookie;

	$in_admincookie = $_COOKIE[$admincookie["name"]];

	if (strlen($in_admincookie)>0)
	{
 	$result = @setadmincookie($admincookie["name"],false,time() - 3600,$admincookie["path"],$admincookie["domain"]);
	}

	return $result;
}

function get_user_id()
{
	global $admincookie;


  if (isset($_COOKIE[$admincookie["name"]]) && $_COOKIE[$admincookie["name"]] != "")
	{
	$in_admincookie =  @$_COOKIE[$admincookie['name']];

	if (isset($in_admincookie) && $in_admincookie != "")
	{
		$decoded_admincookie = feistel_decrypt($in_admincookie);
		$exploded_admincookie = explode(";",$decoded_admincookie);
		return $exploded_admincookie[1];
	}
	else
	{
		return false;
	}
  }
  	else
	{
		return false;
	}
  
}

function get_user_name($in_user_id)
{
	global $conn;

	$result = false;

	return $result;
}

function get_user_level()
{
	global $admincookie;

	$in_admincookie = @$_COOKIE[$admincookie["name"]];

	if (isset($in_admincookie) && $in_admincookie != "")
	{
		$decoded_admincookie = feistel_decrypt($in_admincookie);
		$exploded_admincookie = explode(";",$decoded_admincookie);
		return $exploded_admincookie[2];
	}
	else
	{
		return false;
	}
}

function get_user_level_name()
{
	global $conn;

	$result = false;

	return $result;
}

function get_user_firm()
{
	global $admincookie;

	$in_admincookie = $_COOKIE[$admincookie["name"]];

	if (isset($in_admincookie) && $in_admincookie != "")
	{
		$decoded_admincookie = feistel_decrypt($in_admincookie);
		$exploded_admincookie = explode(";",$decoded_admincookie);
		return $exploded_admincookie[3];
	}
	else
	{
		return false;
	}
}

function get_user_name_by_id($in_id, $reversed = false, $separator = ", ")
{
	global $conn;

	$link=@mysql_connect($conn["host"],$conn["user"],$conn["pass"]);
	if (!$link)
	{
		return false;
	}

	$db=mysql_select_db($conn["db"], $link);

	if (!$db)
	{
		return false;
	}

	$query = "Select USurname, UName from tbluser where ID=".$in_id;
	$res = @mysql_query($query);

	if ($res && @mysql_num_rows($res)>0)
	{
		$resarr = @mysql_fetch_array($res);
		if ($reversed)
		{
			return $resarr["USurname"].$separator.$resarr["UName"];
		}
		else
		{
			return $resarr["UName"]." ".$resarr["USurname"];
		}
	}
	else
	{
		return false;
	}

}


?>
