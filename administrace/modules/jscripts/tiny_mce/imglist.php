<?php 
$output = '';
$delimiter = "\n";
$output .= 'var tinyMCEImageList = new Array(';
$directory = "../pictures"; 
// Since TinyMCE3.x you need absolute image paths in the list...
include "../sql/db.php,"; 
if (is_dir($directory)) {
    $direc = opendir($directory);
    while ($file = readdir($direc)) {
        if (!preg_match('~^\.~', $file)) { // no hidden files / directories here...
            if (is_file("$directory/$file")) {
                // We got ourselves a file! Make an array entry:
                $output .= $delimiter
                    . '["'
                    . utf8_encode($file)
                    . '", "'
                    . utf8_encode("$tempdir/$directory/$file")
                    . '"],';
            }
        }
    }
    $output = substr($output, 0, -1); // remove last comma from array item list (breaks some browsers)
    $output .= $delimiter;
    closedir($direc);
}
$output .= ');'; 
//header('Content-type: text/javascript'); // Make output a real JavaScript file!
echo $output;
?>
