<?php

require_once("../../lib/globals19.php");
include("../lib/user_functions.php");

if (user_logged_in())
{
	user_logout();
}	

 header("Location: ../index.php");
?>
