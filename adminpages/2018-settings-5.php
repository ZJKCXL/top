<?php

 
$nr = 1;
$defother = "Zde můžete uvést vysvětlující či doplňující informace";
$mofotherstyle = 'compact';
$mofmainstyle = 'hibox_diverzita';

//$insertTitle[$nr]  = "Podpora komunity jako součást firemní strategie CSR";
$mofkat[$nr] =  $mofmainstyle ; 
 
//$insertTitle[$nr]  = "Vybraný projekt v oblasti podpory komunity";
//$mofkat[$nr] =  $mofmainstyle ; 

$mofsubtitle[$nr] = 'Název projektu:';
$moftype[$nr] = 7;
$mofnr[$nr] = 1;
$mofhelp[$nr] = "Maximální rozsah 100 znaků včetně mezer." ;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;

$nr++;

$mof[$nr]  = "Jakou má vaše firma strategii v oblasti diverzity? "; 
$mof2help[$nr]  = "Až 20 bodů může získat firma za komplexní, dlouhodobou a promyšlenou strategii "; 
$mofnr[$nr] =2;
$moftype[$nr] = 3;
$limit[$nr] = 1000;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$nr++;  

$mof[$nr]  = "Je vaše firma signatářem Charty Diverzity?   ";  
$mofnr[$nr] =3;
$moftype[$nr] = 1;
$mofkolik[$nr] = 2;
$mofq[$nr][1]	= "ANO";	
$mofq[$nr][2]	= "NE";	
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$nr++;  


 	
$mof[$nr]  = "Stručný popis vybraného projektu"; 
$mofhelp[$nr]  = "Stručně a výstižně popište váš projekt jako celek. U projektů, které se dostanou do užšího výběru, bude tento popis zveřejněn při vyhlašování výsledků a v médiích.  Popis tedy formulujte s ohledem na tuto skutečnost. Uveďte:  
<ul><li><span>důvody / motivaci k realizaci projektu, </span></li> 
<li><span>celkové trvání a vývoj projektu,</span></li> 
<li><span>způsob realizace jednotlivých aktivit, </span></li>
<li><span>dosažené výsledky a jejich význam pro společnost i firmu a její zaměstnance. </span> </li></ul>
";
$mofnr[$nr] =4;
$moftype[$nr] = 3;
$limit[$nr] = 1000;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$nr++;  

 
$mof[$nr]  = "Cíle projektu"; 
$mofhelp[$nr]  = "Popište:    
<ul><li><span>na jaké cílové skupiny zaměstnanců se projekt zaměřuje a proč (lidé s určitou formou znevýhodnění, senioři, rodiče apod.) a jaké problémy řeší,</span> </li>
<li><span>jaké jsou cíle projektu pro firmu jako takovou – tj. proč se firma rozhodla realizovat právě daný projekt </span> </li>
<li><span>jaké jsou konkrétní cíle projektu v oblasti diverzity </span> </li>
<li><span>na základě jakých kritérií posuzujete naplnění cílů projektu.</span> </li>

</ul>
";
$mofnr[$nr] = 5;
$moftype[$nr] = 3;
$limit[$nr] = 1500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 10;
$nr++;  

$mof[$nr]  = "Zdroje pro realizaci projektu"; 
$mofhelp[$nr]  = "Popište, jaké zdroje pro realizaci projektu máte k dispozici:     
<ul><li><span>finanční zdroje (alokované finanční prostředky), </span></li>
<li><span>lidské zdroje (úvazky na řízení a koordinaci projektu apod.),</span></li>
<li><span>materiální zdroje (využívání technického vybavení či prostor apod.),</span></li> 
<li><span>ostatní zdroje (př. know-how, zboží a služby poskytnuté zdarma v rámci spolupráce s partnery apod.). </span></li>
<li><span>podařilo se vám pro projekt získat finance nebo podporu i z jiných než vašich interních zdrojů? </span></li>
</ul>
";
$mofnr[$nr] = 6;
$moftype[$nr] = 3;
$limit[$nr] = 1500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 2.5;
$nr++; 
 	
$mof[$nr]  = "Průběh projektu"; 
$mofhelp[$nr]  = "Popište způsob realizace projektu - tj. jak projekt ve vaší firmě prakticky funguje:     
<ul><li><span>jaké aktivity v rámci projektu realizujete,</span></li>
<li><span>jak pro účast v projektu oslovujete a motivujete zaměstnance,</span></li>
<li><span>jakým způsobem je do projektu zapojeno vedení firmy,</span></li>
<li><span>jak je projekt propojen se strategií řízení lidských zdrojů, </span></li>
<li><span>jak v rámci projektu spolupracujete s partnery, </span></li>
<li><span>s jakými problémy jste se v rámci realizace projektu potýkali a jak jste tyto problémy vyřešili.</span></li>

</ul>
";
$mofnr[$nr] = 7;
$moftype[$nr] = 3;
$limit[$nr] = 2500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 7.5;
$nr++; 

 
$mof[$nr]  = "Dosažené výsledky"; 
$mofhelp[$nr]  = "Popište, čeho jste v průběhu realizace projektu dosáhli. Podařilo se vám naplnit stanovené cíle? Uveďte okamžité výstupy i výsledky či dopady v dlouhodobém časovém horizontu:
<ul><li><span>jaké jsou přímé výstupy vašeho projektu – jaké konkrétní změny jste v rámci realizace projektu dosáhli,</span></li>
<li><span>v jakém rozsahu jste projekt zrealizovali – kolik zaměstnanců se do projektu zapojilo či kolik zaměstnanců projekt ovlivnil, jaký procentuální podíl tito lidé tvoří z celkového počtu zaměstnanců,</span></li> 
<li><span>jaká byla zpětná vazba na projekt – nakolik byly okamžité konkrétní výstupy programu pro vaše zaměstnance přínosné, </span></li>
<li><span>jaký má projekt dlouhodobý přínos pro firmu </span></li>
<li><span>jak výsledky projektu interně i externě komunikujete.   </span></li>

</ul>";
 
$mofnr[$nr] = 8;
$moftype[$nr] = 3;
$limit[$nr] = 2500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 15;
$nr++;

$mof[$nr]  = "Inovace projektu"; 
$mofhelp[$nr]  = "Popište, v čem je váš projekt výjimečný a inovativní, jaká nová řešení jste aplikovali:";
$mofnr[$nr] = 9;
$moftype[$nr] = 3;
$limit[$nr] = 700;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 2;
$nr++;
 
$mof[$nr]  = "Rozvoj projektu"; 
$mofhelp[$nr]  = "Popište, jakým způsobem budete projekt v příštích letech rozvíjet a proč a jak zajistíte jeho dlouhodobou udržitelnost:";
$mofnr[$nr] = 10;
$moftype[$nr] = 3;
$limit[$nr] = 700;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 5.5;
$nr++;   

$mof[$nr]  = "Komentář"; 
$mofhelp[$nr]  = "";
$mof2help[$nr]  = "Komentář nebude bodován";
$moffree[$nr] = 1;
$mofnr[$nr] = 11;
$moftype[$nr] = 3;
$limit[$nr] = 1500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 7.5;
$nr++; 
?>