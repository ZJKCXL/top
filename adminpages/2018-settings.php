<?php
$nr = 1;
$defother = "Zde můžete uvést vysvětlující či doplňující informace";
$mofotherstyle = 'compact';
$mofmainstyle = 'hibox_komunita';
$catname = '2018';

//$insertTitle[$nr]  = "Podpora komunity jako součást firemní strategie CSR";
$mofkat[$nr] =  $mofmainstyle ; 
 
if($partrow['ppValue'] == 3 ) {  $mofmainstyle = 'kat_komunita'; } 
if($partrow['ppValue'] == 4 ) {  $mofmainstyle = 'kat_komunita'; } 
if($partrow['ppValue'] == 6 ) {  $mofmainstyle = 'kat_komunita'; }  
if($partrow['ppValue'] == 5 ) {  $mofmainstyle = 'kat_emplo'; }  
if($partrow['ppValue'] == 7 ) {  $mofmainstyle = 'kat_eko'; } 
 
//$insertTitle[$nr]  = "Vybraný projekt v oblasti podpory komunity";
//$mofkat[$nr] =  $mofmainstyle ; 

$mofsubtitle[$nr] = 'Název projektu:';
$moftype[$nr] = 7;
$mofnr[$nr] = 1;

$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;

$nr++;

 
 	
$mof[$nr]  = "Stručný popis vybraného projektu"; 
$mofhelp[$nr]  = "Stručně a výstižně popište váš projekt jako celek. U projektů, které se dostanou do užšího výběru, bude tento popis zveřejněn při vyhlašování výsledků a v médiích.  Popis tedy formulujte s ohledem na tuto skutečnost. Uveďte:  
 
<ul><li><span>důvody / motivaci k realizaci projektu, </span></li> 
<li><span>celkové trvání a vývoj projektu,</span></li> 
<li><span>způsob realizace jednotlivých aktivit, </span></li>
<li><span>dosažené výsledky a jejich význam pro společnost i firmu a její zaměstnance. </span> </li></ul>
";
$mofnr[$nr] =2;
$moftype[$nr] = 3;
$limit[$nr] = 1000;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$nr++;  

 
$mof[$nr]  = "Cíle projektu"; 
$mofhelp[$nr]  = "Popište:    
<ul><li><span>na jaké cílové skupiny či tematické oblasti se projekt zaměřuje a proč (lidé s určitou formou znevýhodnění, senioři, talentovaní studenti apod.) a jaké společenské či místní problémy řeší,</span> </li>
<li><span>jaké jsou konkrétní cíle projektu v oblasti řešení společenských problémů a zlepšování kvality života,  </span> </li>
<li><span>jaké jsou cíle projektu pro firmu jako takovou - tj. proč se firma rozhodla realizovat právě daný projekt (zlepšení vztahů s klienty a dodavateli, zvýšení produktivity práce, zlepšení reputace firmy a jejího postavení na trhu práce, apod.). </span> </li>
<li><span>na základě jakých kritérií posuzujete naplnění cílů projektu.</span> </li>
</ul>
";
$mofhelp7[$nr]  = "Popište:      
<ul><li><span>proč jste se rozhodli projekt realizovat – jaký je jeho záměr a jaký problém v oblasti ochrany životního prostředí řeší, </span></li>
<li><span>jaké jsou konkrétní cíle projektu, </span></li>
<li><span>jak měříte míru dosažení cílů a jaké indikátory k tomu využíváte (např. ekologická stopa), </span></li>
<li><span>jaké cílové skupiny osob se do projektu zapojují (zaměstnanci, zákazníci, obchodní partneři atd.) a proč.</span></li>
</ul>
";
$mofhelp4[$nr]  = "Popište:     
<ul>
<li><span>na jaké cílové skupiny či tematické oblasti se projekt zaměřuje a proč (lidé s určitou formou znevýhodnění, senioři, životní prostředí apod.), jaké společenské či místní problémy řeší a s jakým výsledkem,</span></li>
<li><span>jakou formu pomoci zaměstnanci poskytují, zda je obsahem projektu odborná pomoc veřejně prospěšným organizacím, na jaké konkrétní oblasti je tato pomoc zaměřena a proč (projektové řízení, strategické plánování, právní, finanční, marketingové poradenství apod.),</span></li> 
<li><span>jaké jsou cíle projektu pro firmu jako takovou - tj. proč se firma rozhodla realizovat právě daný projekt (zvýšení produktivity práce, zlepšení reputace firmy a jejího postavení na trhu práce, snížení fluktuace, snížení absencí z důvodu nemoci, snížení nákladů na přijímání zaměstnaných či vzdělávání apod.), </span></li>
<li><span>jaké jsou konkrétní cíle projektu v oblasti zaměstnanecké politiky a řízení lidských zdrojů (zvýšení spokojenosti a motivovanosti zaměstnaných, rozvoj kompetencí a dovedností zaměstnaných, apod.).</span></li>


</ul>
";

$mofnr[$nr] = 3;
$moftype[$nr] = 3;
$limit[$nr] = 1500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 10;
$nr++;  

$mof[$nr]  = "Zdroje pro realizaci projektu"; 
$mofhelp[$nr]  = "Popište, jaké zdroje pro realizaci projektu máte k dispozici:     
<ul><li><span>finanční zdroje (alokované finanční prostředky), </span></li>
<li><span>lidské zdroje (úvazky na řízení a koordinaci projektu apod.),</span></li>
<li><span>materiální zdroje (využívání technického vybavení či prostor apod.),</span></li> 
<li><span>ostatní zdroje (př. know-how, zboží a služby poskytnuté zdarma v rámci spolupráce s partnery apod.). </span></li>
<li><span>podařilo se vám pro projekt získat finance nebo podporu i z jiných než vašich interních zdrojů? </span></li>
</ul>
";
$mofhelp7[$nr]  = "Popište, jaké zdroje pro realizaci projektu máte k dispozici:     
<ul><li><span>finanční zdroje (alokované finanční prostředky), </span></li>
<li><span>lidské zdroje (úvazky na řízení a koordinaci projektu apod.),</span></li>
<li><span>materiální zdroje (využívání technického vybavení či prostor apod.),</span></li> 
<li><span>ostatní zdroje (př. know-how, zboží a služby poskytnuté zdarma v rámci spolupráce s partnery apod.). </span></li>
</ul>
";
$mofhelp4[$nr]  = "Popište, jaké zdroje pro realizaci projektu máte k dispozici:     
<ul><li><span>finanční zdroje (alokované finanční prostředky), </span></li>
<li><span>lidské zdroje (úvazky na řízení a koordinaci projektu apod.),</span></li>
<li><span>materiální zdroje (využívání technického vybavení či prostor apod.),</span></li> 
<li><span>ostatní zdroje (př. know-how, zboží a služby poskytnuté zdarma v rámci spolupráce s partnery apod.). </span></li>
</ul>
";

$mofnr[$nr] = 4;
$moftype[$nr] = 3;
$limit[$nr] = 1500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 2.5;
$nr++; 
 	
$mof[$nr]  = "Průběh projektu"; 
$mofhelp[$nr]  = "Popište způsob realizace projektu - tj. jak projekt ve vaší firmě prakticky funguje:     
<ul><li><span> jaké aktivity v rámci projektu realizujete, </span></li>
<li><span>jakým způsobem je do projektu zapojeno vedení firmy a zaměstnanci,</span></li>
<li><span>jak oslovujete cílové skupiny,  </span></li>
<li><span>zda a jak pro účast v projektu oslovujete a motivujete zákazníky,</span></li> 
<li><span>jak v rámci projektu spolupracujete s veřejně prospěšnými organizacemi či dalšími partnery, </span></li>
<li><span>s jakými problémy jste se v rámci realizace projektu potýkali a jak jste tyto problémy vyřešili.</span></li>
</ul>
";

$mofhelp3[$nr]  = "Popište způsob realizace projektu - tj. jak to ve vaší firmě prakticky funguje:      
<ul><li><span> jaké aktivity v rámci projektu realizujete, </span></li>
<li><span>jakým způsobem je do projektu zapojeno vedení firmy a zaměstnanci,</span></li>
<li><span>jak v rámci projektu spolupracujete s veřejně prospěšnými organizacemi, školami či dalšími partnery,  </span></li>
<li><span>s jakými problémy jste se v rámci realizace projektu potýkali a jak jste tyto problémy vyřešili.</span></li>
</ul>
";

$mofhelp7[$nr]  = "Popište způsob realizace projektu - tj. jak to ve vaší firmě prakticky funguje:      
<ul>
<li><span>jaké aktivity realizujete,</span></li>
<li><span>jak se zapojují cílové skupiny (osvěta a vzdělávání, využití know-how, atd.), jak je oslovujete a motivujete,</span></li>
<li><span>jakým způsobem je zapojeno vedení firmy a zaměstnanci,</span></li>
<li><span>jak spolupracujete s partnery, </span></li>
<li><span>s jakými problémy jste se potýkali a jak jste tyto problémy vyřešili.</span></li>
</ul>
";
$mofhelp4[$nr]  = "Popište způsob realizace projektu - tj. jak projekt ve vaší firmě prakticky funguje:     
<ul><li><span>jaké aktivity v rámci projektu realizujete,</span></li>
<li><span>jak pro účast v projektu oslovujete a motivujete zaměstnance,</span></li>
<li><span>jak motivujete zaměstnance pro odbornou pomoc, </span></li>
<li><span>jaké konkrétní aktivity zaměstnanci vykonávají,</span></li>
<li><span>jaký podíl z celkově realizované pomoci tvoří odborná pomoc, </span></li>
<li><span>jakým způsobem je do projektu zapojeno vedení firmy,</span></li>
<li><span>jak je projekt propojen se strategií řízení lidských zdrojů a s plánem firemního vzdělávání,</span></li>
<li><span>jak v rámci projektu spolupracujete s veřejně prospěšnými organizacemi a partnery, </span></li>
<li><span>s jakými problémy jste se v rámci realizace projektu potýkali a jak jste tyto problémy vyřešili.</span></li>

</ul>
";

$mofnr[$nr] = 5;
$moftype[$nr] = 3;
$limit[$nr] = 2500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 7.5;
$nr++; 

 
$mof[$nr]  = "Dosažené výsledky"; 
$mofhelp[$nr]  = "Popište, čeho jste v průběhu realizace projektu dosáhli.  Podařilo se vám naplnit stanovené cíle? Uveďte okamžité výstupy i výsledky či dopady v dlouhodobém časovém horizontu:
<ul><li><span>jaké jsou přímé výstupy vašeho projektu a přínosy pro společnost a místní komunitu - jaké konkrétní změny jste v rámci realizace projektu dosáhli, v čem konkrétně jste pomohli společnosti, místní komunitě či veřejně prospěšnému sektoru,</span></li> 
<li><span>v jakém rozsahu jste projekt zrealizovali,</span></li> 
<li><span>jaká byla zpětná vazba na projekt ze strany partnerských organizací, </span></li>
<li><span>jaký má projekt dlouhodobý přínos pro firmu a dopad na rozvoj udržitelného podnikání, </span></li>
<li><span>jak výsledky projektu interně i externě komunikujete.  </span></li>
</ul>";

$mofhelp3[$nr]  = "Popište, čeho jste dosáhli. Uveďte okamžité výstupy i výsledky či dopady v dlouhodobém časovém horizontu:
<ul><li><span>jaké jsou přímé výstupy vašeho projektu a přínosy pro společnost a místní komunitu - jaké konkrétní změny jste v rámci realizace projektu dosáhli, v čem konkrétně jste pomohli společnosti, místní komunitě či veřejně prospěšnému sektoru,</span></li> 
<li><span>v jakém rozsahu jste projekt zrealizovali, </span></li> 
<li><span>jaká byla zpětná vazba na projekt ze strany partnerských organizací, </span></li>
<li><span>jaký má projekt dlouhodobý přínos pro firmu a dopad na rozvoj udržitelného podnikání, </span></li>
<li><span>jak výsledky projektu interně i externě komunikujete.    </span></li>
</ul>";
 
$mofhelp7[$nr]  = "Popište, čeho jste dosáhli. Uveďte okamžité výstupy i výsledky či dopady v dlouhodobém časovém horizontu:     
<ul>
<li><span>jaké jsou přímé výstupy vašeho projektu, </span></li>
<li><span>zda se vám podařilo dosáhnout stanovených cílů, příp. do jaké míry, </span></li>
<li><span>jaké jsou přínosy pro firmu (např. výstupy pozitivně ovlivňují odbyt produktů či služeb firmy, zlepšují image a postavení firmy na trhu, snižují náklady na činnost firmy atd.), </span></li>
<li><span>jaká byla zpětná vazba (od cílové skupiny, zaměstnanců, veřejnosti, atd.) </span></li>
<li><span>jaký má projekt dlouhodobý přínos pro firmu a dopad na rozvoj udržitelného podnikání, </span></li>
<li><span>jak výsledky interně i externě komunikujete.  </span></li>
</ul>
";
$mofhelp4[$nr]  = "Popište, čeho jste v průběhu realizace projektu dosáhli. Podařilo se vám naplnit stanovené cíle? Uveďte okamžité výstupy i výsledky či dopady v dlouhodobém časovém horizontu:    
<ul>
<li><span>jaké jsou přímé výstupy vašeho projektu – jaké konkrétní změny jste v rámci realizace projektu dosáhli, v čem konkrétně jste pomohli veřejně prospěšnému sektoru (profesionalizace managementu organizací, podpora realizovaných projektů, úpravy prostor, přínos pro klienty apod.), </span></li>
<li><span>v jakém rozsahu jste projekt zrealizovali – kolik jste zrealizovali akcí, kolik zaměstnaných se do projektu zapojilo, jaký procentuální podíl tito lidé tvoří z celkového počtu zaměstnanců, kolik hodin práce jste věnovali veřejně prospěšnému sektoru, kolika osobám / organizacím jste v rámci projektu pomohli, </span></li>
<li><span>jaké jsou výsledky a dopady odborné pomoci, </span></li>
<li><span>jaká byla zpětná vazba na projekt – nakolik byly okamžité konkrétní výstupy přínosné pro veřejně prospěšné organizace, jejich klienty a vaše zaměstnance, </span></li>
<li><span>jak zapojení do projektu ovlivnilo vaše zaměstnance (zda získali nové dovednosti, rozvinuly se jejich osobní hodnoty, zvýšila se jejich motivace pro práci pro vaši firmu apod.),</span></li>
<li><span>jaký má projekt dlouhodobý přínos pro firmu a dopad na rozvoj udržitelného podnikání (zda se zlepšila motivovanost zaměstnaných, zlepšila produktivita práce, snížila fluktuace apod.),</span></li>
<li><span>jak výsledky projektu interně i externě komunikujete.  </span></li>
</ul>
";

$mofnr[$nr] = 6;
$moftype[$nr] = 3;
$limit[$nr] = 2500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 15;
$nr++;

$mof[$nr]  = "Inovace projektu"; 
$mofhelp[$nr]  = "Popište, v čem je váš projekt výjimečný a inovativní, jaká nová řešení jste aplikovali:";
$mofnr[$nr] = 7;
$moftype[$nr] = 3;
$limit[$nr] = 700;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 2;
$nr++;
 
$mof[$nr]  = "Rozvoj projektu"; 
$mofhelp[$nr]  = "Popište, jakým způsobem budete projekt v příštích letech rozvíjet a proč a jak zajistíte jeho dlouhodobou udržitelnost:";
$mofnr[$nr] = 8;
$moftype[$nr] = 3;
$limit[$nr] = 700;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 5.5;
$nr++;   

$mof[$nr]  = "Komentář"; 
$mof2help[$nr]  = "Komentář nebude bodován";
$mofhelp[$nr]  = "";
$moffree[$nr] = 1;
$mofnr[$nr] = 9;
$moftype[$nr] = 3;
$limit[$nr] = 1500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 7.5;

$nr++; 
?>