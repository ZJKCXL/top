<?php
$nr = 1;
$defother = "Zde můžete uvést vysvětlující či doplňující informace";
$mofotherstyle = 'compact';
$mofmainstyle = 'spoecialni';
$catname = 'POČIN ROKU 2017';


$insertTitle[$nr]  = "Nominační formulář ";
$mofkat[$nr] =  $mofmainstyle ;
 
$mof[$nr] = 'Název nominovaného projektu/firmy: ';
$mofnr[$nr] = '1';
$moftype[$nr] = 7;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 0; 
 $mofhod[$nr]= 'next';
  $nr++;

  $mof[$nr] = 'Proč projekt/aktivitu/nápad nominujete? Proč si myslíte, že by měl získat titul Počin roku? ';
$mofnr[$nr] = '2';
$moftype[$nr] = 3;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftype[$nr] = 3;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 0; 
 $mofhod[$nr]= 'next';
  $nr++;

    $mof[$nr] = 'Kontaktní údaje na nominovanou společnost/zaměstnance (případně společnost, ve které působí): ';
$mofnr[$nr] = '3';
$moftype[$nr] = 3;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftype[$nr] = 3;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 0; 
 $mofhod[$nr]= 'next';
  $nr++;

    $mof[$nr] = 'Jméno nominující organizace/firmy; kontaktní osoba:  ';
$mofnr[$nr] = '4';
$moftype[$nr] = 3;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftype[$nr] = 3;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 0; 
 $mofhod[$nr]= 'next';
  $nr++;

 
 ?>