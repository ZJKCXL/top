<?php
$nr = 1;
$defother = "Zde můžete uvést vysvětlující či doplňující informace";
$mofotherstyle = 'compact';
$mofmainstyle = 'hibox_komunita';
$catname = 'NEJANGAŽOVANĚJŠÍ ZAMĚSTNANCI 2017';


//$insertTitle[$nr]  = "Pracovní prostředí a vztahy jako součást firemní strategie CSR";
//$mofkat[$nr] =  $mofmainstyle ;
$nr++;

 
 
$mof[$nr]  = "Název projektu";
$mofnr[$nr] = 1;
$moftype[$nr] = 7;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofqhodweight[$nr]	= 0;	
$nr++;

$mof[$nr]  = "Stručný popis projektu"; 
$mofnr[$nr] = '2';
$moftype[$nr] = 3;
$moffree[$nr] = 0;
$mofhelp[$nr]  = "Stručně a výstižně popište váš projekt jako celek. U projektů, které se dostanou užšího výběru, bude tento popis zveřejněn při vyhlašování výsledků a v médiích.  Popis tedy formulujte s ohledem na tuto skutečnost. Uveďte:  
<ul><li><span>důvody / motivaci k realizaci projektu,</span></li> 
<li><span>formy podpory odborné pomoci,</span></li> 
<li><span>celkové trvání a vývoj projektu, </span></li>
<li><span>způsob realizace jednotlivých aktivit, </span></li>
<li><span>dosažené výsledky a jejich význam pro společnost, firmu i její zaměstnance.</span> </li></ul>
";
$limit[$nr] = 1000;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofqhodweight[$nr]	= 0;	
$nr++;   

 

$mof[$nr]  = "Cíle projektu"; 
$mofnr[$nr] = '3';
$moftype[$nr] = 3;
$moffree[$nr] = 0;
$mofhelp[$nr]  = "Popište:  
<ul><li><span>na jaké cílové skupiny či tematické oblasti se projekt zaměřuje a proč (lidé s určitou formou znevýhodnění, senioři, životní prostředí apod.), jaké společenské či místní problémy řeší a s jakým výsledkem,</span></li> 
<li><span>jakou formu pomoci zaměstnanci poskytují, zda je obsahem projektu odborná pomoc veřejně prospěšným organizacím, na jaké konkrétní oblasti je tato pomoc zaměřena a proč (projektové řízení, strategické plánování, právní, finanční, marketingové poradenství apod.), </span></li> 
<li><span>jaké jsou cíle projektu pro firmu jako takovou  tj. proč se firma rozhodla realizovat právě daný projekt (zvýšení produktivity práce, zlepšení reputace firmy a jejího postavení na trhu práce, snížení fluktuace, snížení absencí z důvodu nemoci, snížení nákladů na přijímání zaměstnaných či vzdělávání apod.),  </span></li>
<li><span>jaké jsou konkrétní cíle projektu v oblasti zaměstnanecké politiky a řízení lidských zdrojů (zvýšení spokojenosti a motivovanosti zaměstnaných, rozvoj kompetencí a dovedností zaměstnaných, apod.). </span> </li></ul>
";
$limit[$nr] = 1500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 12.5;
$nr++;   
 


$mof[$nr]  = "Zdroje pro realizaci projektu"; 
$mofnr[$nr] = '4';
$moftype[$nr] = 3;
$moffree[$nr] = 0;
$mofhelp[$nr]  = "Popište, jaké zdroje pro realizaci projektu máte k dispozici:  
<ul><li><span>finanční zdroje (alokované finanční prostředky),</span></li> 
<li><span>lidské zdroje (úvazky na řízení a koordinaci projektu apod.),</span></li> 
<li><span>materiální zdroje (využívání technického vybavení či prostor apod.), </span></li>
<li><span>ostatní zdroje (př. know-how, zboží a služby poskytnuté zdarma v rámci spolupráce s partnery apod.).</span> </li></ul>
";
$limit[$nr] = 1500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	=  2.5;
$nr++;   
 

$mof[$nr]  = "Průběh projektu"; 
$mofnr[$nr] = '5';
$moftype[$nr] = 3;
$moffree[$nr] = 0;
$mofhelp[$nr]  = "Popište způsob realizace projektu - tj. jak projekt ve vaší firmě prakticky funguje:   
<ul><li><span>jaké aktivity v rámci projektu realizujete,  </span> </li>
<li><span>jak pro účast v projektu oslovujete a motivujete zaměstnance,</span> </li>
<li><span>jak motivujete zaměstnance pro odbornou pomoc, </span> </li>
<li><span>jaké konkrétní aktivity zaměstnanci vykonávají, </span> </li>
<li><span>jaký podíl z celkově realizované pomoci tvoří odborná pomoc, </span> </li>
<li><span>jakým způsobem je do projektu zapojeno vedení firmy, </span> </li>
<li><span>jak je projekt propojen se strategií řízení lidských zdrojů a s plánem firemního vzdělávání,  </span> </li>
<li><span>jak v rámci projektu spolupracujete s veřejně prospěšnými organizacemi a partnery,  </span> </li>
<li><span>s jakými problémy jste se v rámci realizace projektu potýkali a jak jste tyto problémy vyřešili.
</span> </li></ul>
";
$limit[$nr] = 2500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 7.5;
$nr++;   
 
 
$mof[$nr]  = "Dosažené výsledky"; 
$mofnr[$nr] = '6';
$moftype[$nr] = 3;
$moffree[$nr] = 0;
$mofhelp[$nr]  = "Popište, čeho jste v průběhu realizace projektu dosáhli. Podařilo se vám naplnit stanovené cíle? Uveďte okamžité výstupy i výsledky či dopady v dlouhodobém časovém horizontu:   
<ul><li><span> jaké jsou přímé výstupy vašeho projektu  jaké konkrétní změny jste v rámci realizace projektu dosáhli, v čem konkrétně jste pomohli veřejně prospěšnému sektoru (profesionalizace managementu organizací, podpora realizovaných projektů, úpravy prostor, přínos pro klienty apod.), </span></li>
<li><span>v jakém rozsahu jste projekt zrealizovali  kolik jste zrealizovali akcí, kolik zaměstnaných se do projektu zapojilo, jaký procentuální podíl tito lidé tvoří z celkového počtu zaměstnanců, kolik hodin práce jste věnovali veřejně prospěšnému sektoru, kolika osobám / organizacím jste v rámci projektu pomohli,    </span></li>
<li><span>jaké jsou výsledky a dopady odborné pomoci,   </span></li>
<li><span>jaká byla zpětná vazba na projekt  nakolik byly okamžité konkrétní výstupy přínosné pro veřejně prospěšné organizace, jejich klienty a vaše zaměstnance,   </span></li>
<li><span>jak zapojení do projektu ovlivnilo vaše zaměstnance (zda získali nové dovednosti, rozvinuly se jejich osobní hodnoty, zvýšila se jejich motivace pro práci pro vaši firmu apod.),  </span></li>
<li><span>jaký má projekt dlouhodobý přínos pro firmu a dopad na rozvoj udržitelného podnikání (zda se zlepšila motivovanost zaměstnaných, zlepšila produktivita práce, snížila fluktuace apod.),    </span></li>
<li><span>jak výsledky projektu interně i externě komunikujete.  
</span> </li></ul>
";
$limit[$nr] = 2500;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 15;
$nr++;  

 
$mof[$nr]  = "Inovace projektu"; 
$mofnr[$nr] = '7';
$moftype[$nr] = 3;
$moffree[$nr] = 0;
$mofhelp[$nr]  = "Popište, v čem je váš projekt výjimečný a inovativní, jaká nová řešení jste aplikovali: ";
$limit[$nr] = 700;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 5;
$nr++; 

 
$mof[$nr]  = "Rozvoj projektu"; 
$mofnr[$nr] = '8';
$moftype[$nr] = 3;
$moffree[$nr] = 0;
$mofhelp[$nr]  = "Popište, jakým způsobem budete projekt v příštích letech rozvíjet a proč a jak zajistíte jeho dlouhodobou udržitelnost:";
$limit[$nr] = 700;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 7.5;
$nr++; 
 ?>