<?php
$nr = 1;
$defother = "Zde můžete uvést vysvětlující či doplňující informace";
$mofotherstyle = 'compact';
$mofmainstyle = 'hibox_green';
$catname = 'LEADER V ŽIVOTNÍM PROSTŘEDÍ 2017';
$insertTitle[$nr]  = "	Ochrana životního prostředí jako součást firemní strategie CSR ";
$mofkat[$nr] =  $mofmainstyle ;
 
$mof[$nr] = "Je ochrana životního prostředí zakotvena ve strategických dokumentech vaší firmy?";
$mofnr[$nr] = 1;
$moftype[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;
$mofkolik[$nr] = 4;
$mofq[$nr][1]	= "ANO, v plném rozsahu";	
$mofq[$nr][2]	= "ANO, rozpracováno / částečně";	
$mofq[$nr][3]	= "NE, v plánu pro příští období";	
$mofq[$nr][4]	= "NE, není pro nás aktuálně relevantní";
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 0; 
$mofhod[$nr]= 'auto';
$mofqhodpoints[$nr][1]	= 4;	
$mofqhodpoints[$nr][2]	= 2;	
$mofqhodpoints[$nr][3]	= 0;	
$mofqhodpoints[$nr][4]	= 0;
$mofqhodweight[$nr]	= 2;	
$nr++;
$mofsubtitle[$nr]  = "<br/>Pokud ano, popište, o jaké dokumenty se jedná a jak je rozvíjíte, aktualizujete a pracujete s nimi. "; 
$mofnr[$nr] = 2;
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 10000000;                                         
$moffilename[$nr] = 'file'.$nr;
$mofnextcat[$nr] =0; 
$moflastcat[$nr] = 1; 
$mofkat[$nr] = $mofmainstyle;
$moffree[$nr] = '1';    
$nr++;  

/*
$mof[$nr]  = "Uveďte, jaké jsou vaše klíčové priority a cíle.";  
$mofnr[$nr] = 2;
$moftype[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;                               
$mofkolik[$nr] = 4;  
$mofkat[$nr] = $mofmainstyle;
  
$mofsubtitle[$nr] = 'Priorita 1';
$moftype[$nr] = 7;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;       
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 0; 
  $nr++;
$mofsubtitle[$nr] = 'Cíl 1';
$moftype[$nr] = 7;
$moffree[$nr] = 0;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
   $nr++;
$mofsubtitle[$nr] = 'Cíl 2';
$moftype[$nr] = 7;
$moffree[$nr] = 1;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Cíl 3';
$moftype[$nr] = 7;
$moffree[$nr] = 1;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Priorita 2';
$moftype[$nr] = 7;
$moffree[$nr] = 1;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Cíl 1';
$moftype[$nr] = 7;
$mofhelp[$nr]  = "";
$moffree[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Cíl 2';
$moftype[$nr] = 7;
$moffree[$nr] = 1;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Cíl 3';
$moftype[$nr] = 7;
$moffree[$nr] = 1;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = 'hibox_work';
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr]  = "<br/>Zde můžete uvést vysvětlující či doplňující informace"; 
$mofnr[$nr] = 0;
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 10000000;                                               
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 0; 
$moflastcat[$nr] = 1;                                                    
$moffree[$nr] = '1';    $nr++;  
$mof[$nr] = "Je určen konkrétní člen top managementu, který nese odpovědnost za tuto oblast? ";
$mofnr[$nr] = 3;
$moftype[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;
$mofkolik[$nr] = 4;
$mofq[$nr][1]	= "ANO, v plném rozsahu";	
$mofq[$nr][2]	= "ANO, rozpracováno / částečně";	
$mofq[$nr][3]	= "NE, v plánu pro příští období";	
$mofq[$nr][4]	= "NE, není pro nás aktuálně relevantní";
$mofkat[$nr] = $mofmainstyle;
$mofothers[$nr] =  $defother;	
$moffilenameothers[$nr] = 'otherfile'.$nr;
$mofhod[$nr]= 'auto';
$mofqhodpoints[$nr][1]	= 4;	
$mofqhodpoints[$nr][2]	= 2;	
$mofqhodpoints[$nr][3]	= 0;	
$mofqhodpoints[$nr][4]	= 0;
$mofqhodweight[$nr]	= 2.5;	
 $nr++;
 
$mof[$nr] = "Jsou na nižších článcích vašeho managementu stanoveny odpovědnosti za dílčí témata v této oblasti? ";
$mofnr[$nr] = 4;
$moftype[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;
$mofkolik[$nr] = 4;
$mofq[$nr][1]	= "ANO, v plném rozsahu";	
$mofq[$nr][2]	= "ANO, rozpracováno / částečně";	
$mofq[$nr][3]	= "NE, v plánu pro příští období";	
$mofq[$nr][4]	= "NE, není pro nás aktuálně relevantní";
$mofkat[$nr] = $mofmainstyle;
$mofothers[$nr] =  $defother;	
$moffilenameothers[$nr] = 'otherfile'.$nr;
$mofhod[$nr]= 'auto';
$mofqhodpoints[$nr][1]	= 4;	
$mofqhodpoints[$nr][2]	= 2;	
$mofqhodpoints[$nr][3]	= 0;	
$mofqhodpoints[$nr][4]	= 0;
$mofqhodweight[$nr]	= 1.5;	
$nr++;
 
  
$mof[$nr] = "Promítá se téma do hodnocení vedoucích pracovníků a pracovnic?";
$mofnr[$nr] = 5;
$moftype[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;
$mofkolik[$nr] = 4;
$mofq[$nr][1]	= "ANO, v plném rozsahu";	
$mofq[$nr][2]	= "ANO, rozpracováno / částečně";	
$mofq[$nr][3]	= "NE, v plánu pro příští období";	
$mofq[$nr][4]	= "NE, není pro nás aktuálně relevantní";
$mofkat[$nr] = $mofmainstyle;
$mofothers[$nr] =  $defother;	
$moffilenameothers[$nr] = 'otherfile'.$nr;
$mofhod[$nr]= 'auto';
$mofqhodpoints[$nr][1]	= 4;	
$mofqhodpoints[$nr][2]	= 2;	
$mofqhodpoints[$nr][3]	= 0;	
$mofqhodpoints[$nr][4]	= 0;
$mofqhodweight[$nr]	= 1.5;	
 $nr++;

*/
 
$mof[$nr] = "Sledujete / vyhodnocujete přínos své politiky v oblasti ochrany životního prostředí?";
$mofnr[$nr] = '2';
$moftype[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;
$mofkolik[$nr] = 4;
$mofq[$nr][1]	= "ANO, v plném rozsahu";	
$mofq[$nr][2]	= "ANO, rozpracováno / částečně";	
$mofq[$nr][3]	= "NE, v plánu pro příští období";	
$mofq[$nr][4]	= "NE, není pro nás aktuálně relevantní";
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'auto';
$mofqhodpoints[$nr][1]	= 4;	
$mofqhodpoints[$nr][2]	= 2;	
$mofqhodpoints[$nr][3]	= 0;	
$mofqhodpoints[$nr][4]	= 0;
$mofqhodweight[$nr]	= 2.5;	
 $nr++;
 
 /*
$mof[$nr] = "Jedná se o periodický, soustavný proces? ";
$mofnr[$nr] = '7';
$moftype[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;
$mofkolik[$nr] = 4;
$mofq[$nr][1]	= "ANO, v plném rozsahu";	
$mofq[$nr][2]	= "ANO, rozpracováno / částečně";	
$mofq[$nr][3]	= "NE, v plánu pro příští období";	
$mofq[$nr][4]	= "NE, není pro nás aktuálně relevantní";
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'auto';
$mofqhodpoints[$nr][1]	= 4;	
$mofqhodpoints[$nr][2]	= 2;	
$mofqhodpoints[$nr][3]	= 0;	
$mofqhodpoints[$nr][4]	= 0;
$mofqhodweight[$nr]	= 1.5;	
  $nr++; 
$mof[$nr]  = "Uveďte, které parametry sledujete, jaké nástroje pro vyhodnocení využíváte a jakých jste dosáhli výsledků.  "; 
$mofnr[$nr] = '8';
$moftype[$nr] = 3;
$limit[$nr] = 10000000;     
$moffree[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moffree[$nr] = '1';    
$nr++;   
$mof[$nr] = "Je ochrana životního prostředí explicitně obsažena ve vaší výroční zprávě, ve zprávě o udržitelném rozvoji, CSR reportu nebo v jiných firemních materiálech? ";
$mofnr[$nr] = '9';
$moftype[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;
$mofkolik[$nr] = 4;
$mofq[$nr][1]	= "ANO, v plném rozsahu";	
$mofq[$nr][2]	= "ANO, rozpracováno / částečně";	
$mofq[$nr][3]	= "NE, v plánu pro příští období";	
$mofq[$nr][4]	= "NE, není pro nás aktuálně relevantní";
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 0; 
$mofhod[$nr]= 'auto';
$mofqhodpoints[$nr][1]	= 4;	
$mofqhodpoints[$nr][2]	= 2;	
$mofqhodpoints[$nr][3]	= 0;	
$mofqhodpoints[$nr][4]	= 0;
$mofqhodweight[$nr]	= 1;	
  $nr++; 
$mofsubtitle[$nr]  = "<br/>Pokud ano, uveďte, o jaké konkrétní materiály se jedná a jaké informace jsou v nich obsaženy. Případně doplňte odkaz na materiál zveřejněný na webu nebo jinde. "; 
$mofnr[$nr] = '10';
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 10000000;     
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 0; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    
$nr++;  

*/
 
$mof[$nr] = 'Vaše projekty v oblasti ochrany životního prostředí ';
$mofnr[$nr] = '3';
$moftype[$nr] = 1;
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofsubtitle[$nr] = 'Název projektu 1:';
$moftype[$nr] = 7;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 0; 
 $mofhod[$nr]= 'next';
  $nr++;
$mofsubtitle[$nr] = 'Cíl';
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 440;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Trvání';
$moffree[$nr] = 1;
$moftype[$nr] = 7;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Kvantifikované výstupy a výsledky: ';
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 440;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Odkaz / kontakt, kde je možno výsledky ověřit: ';
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 440;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 0; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Název projektu 2:';
$moffree[$nr] = 1;
$moftype[$nr] = 7;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 0; 
$moffree[$nr] = '1';    
 $mofhod[$nr]= 'next';
$nr++;
$mofsubtitle[$nr] = 'Cíl';
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 440;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Trvání';
$moffree[$nr] = 1;
$moftype[$nr] = 7;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Kvantifikované výstupy a výsledky: ';
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 440;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;          
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Odkaz / kontakt, kde je možno výsledky ověřit: ';
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 440;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 0; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Název projektu 3:';
$moffree[$nr] = 1;
$moftype[$nr] = 7;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 0; 
$moffree[$nr] = '1';    
$nr++;
$mofsubtitle[$nr] = 'Cíl';
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 440;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Trvání';
$moffree[$nr] = 1;
$moftype[$nr] = 7;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Kvantifikované výstupy a výsledky: ';
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 440;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 1; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    $nr++;
$mofsubtitle[$nr] = 'Odkaz / kontakt, kde je možno výsledky ověřit: ';
$moffree[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 440;
$mofhelp[$nr]  = "";
$moffilename[$nr] = 'file'.$nr; 
$mofkat[$nr] = $mofmainstyle;
$moftitlekat[$nr] = 'compact ';
$mofnextcat[$nr] = 0; 
$moflastcat[$nr] = 1; 
$moffree[$nr] = '1';    
$mofhod[$nr] = 'extra';
$mofhodtxt[$nr]  = 'Vaše hodnocení celkového přístupu a strategie v oblasti ochrany životního prostředí  '; 
$mofqhodweight[$nr] = 20;
$nr++;
 
$insertTitle[$nr]  = "Vybraný projekt v oblasti ochrany životního prostředí";
$mofkat[$nr] =  $mofmainstyle ;
//$mof[$nr]  = 'Vybraný projekt v oblasti pracovního prostředí a vztahů';
$mofnr[$nr] = '3';
$moftype[$nr] = 7;
$moffilename[$nr] = 'file'.$nr; 
$mofkat[$nr] = $mofmainstyle;
$moffree[$nr] = '1';    $nr++;
 	
$mof[$nr]  = "Stručný popis vybraného projektu"; 
$mofhelp[$nr]  = "Stručně a výstižně popište váš projekt jako celek. U projektů, které se dostanou užšího výběru, bude tento popis zveřejněn při vyhlašování výsledků a v médiích.  Popis tedy formulujte s ohledem na tuto skutečnost. Uveďte: 
 
<ul><li><span>důvody / motivaci k nastavení vašeho projektu, </span></li> 
<li><span>celkové trvání a vývoj projektu,</span></li> 
<li><span>způsob realizace jednotlivých aktivit, </span></li>
<li><span>dosažené výsledky a jejich význam pro firmu a její zaměstnance. </span> </li></ul>
";
$mofnr[$nr] = 1;
$moftype[$nr] = 3;
$limit[$nr] = 10000000;     
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$nr++;  
 
$mof[$nr]  = "Cíle projektu"; 
$mofhelp[$nr]  = "Popište:    
<ul><li><span>proč jste se rozhodli projekt realizovat - jaký je jeho záměr a jaký problém v oblasti ochrany životního prostředí řeší,</span> </li>
<li><span>jaké jsou konkrétní cíle projektu, </span> </li>
<li><span>jak měříte míru dosažení cílů a jaké indikátory k tomu využíváte (např. ekologická stopa),</span> </li>
<li><span>jaké cílové skupiny osob se do projektu zapojují (zaměstnanci, zákazníci, obchodní partneři atd.) a proč.</span> </li>
</ul>
";
$mofnr[$nr] = 2;
$moftype[$nr] = 3;
$limit[$nr] = 10000000;     
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 12.5; 
$nr++;  
$mof[$nr]  = "Zdroje pro realizaci projektu"; 
$mofhelp[$nr]  = "Popište, jaké zdroje pro realizaci projektu máte k dispozici:     
<ul><li><span>finanční zdroje (alokované finanční prostředky), </span></li>
<li><span>lidské zdroje (úvazky na řízení a koordinaci projektu apod.),</span></li>
<li><span>materiální zdroje (využívání technického vybavení či prostor apod.),</span></li> 
<li><span>ostatní zdroje (př. know-how, zboží a služby poskytnuté zdarma v rámci spolupráce s partnery apod.). </span></li>
</ul>
";
$mofnr[$nr] = 3;
$moftype[$nr] = 3;
$limit[$nr] = 10000000;     
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 2.5; 
$nr++; 
 	
$mof[$nr]  = "Průběh projektu"; 
$mofhelp[$nr]  = "Popište způsob realizace projektu tj. jak projekt ve vaší firmě prakticky funguje:     
<ul><li><span>jaké aktivity realizujete, </span></li>
<li><span>jak se zapojují cílové skupiny (osvěta a vzdělávání, využití know-how, atd.), jak je oslovujete a motivujete,</span></li>
<li><span>jakým způsobem je zapojeno vedení firmy a zaměstnanci, </span></li>
<li><span>jak spolupracujete s partnery, </span></li> 
<li><span>s jakými problémy jste se potýkali a jak jste tyto problémy vyřešili. </span></li>
</ul>
";
$mofnr[$nr] = 4;
$moftype[$nr] = 3;
$limit[$nr] = 10000000;     
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 7.5; 
$nr++; 
 
$mof[$nr]  = "Dosažené výsledky"; 
$mofhelp[$nr]  = "Popište, čeho jste dosáhli. Uveďte okamžité výstupy i výsledky či dopady v dlouhodobém časovém horizontu:
<ul><li><span>jaké jsou přímé výstupy vašeho projektu, </span></li> 
<li><span>zda se vám podařilo dosáhnout stanovených cílů, příp. do jaké míry, </span></li> 
<li><span>jaké jsou přínosy pro firmu (např. výstupy pozitivně ovlivňují odbyt produktů či služeb firmy, zlepšují image a postavení firmy na trhu, snižují náklady na činnost firmy atd.), </span></li>
<li><span>jaká byla zpětná vazba (od cílové skupiny, zaměstnanců, veřejnosti, atd.) </span></li>
<li><span>jaký má projekt dlouhodobý přínos pro firmu a dopad na rozvoj udržitelného podnikání, </span></li>
<li><span>jak výsledky interně i externě komunikujete.   </span></li>
</ul>
";
$mofnr[$nr] = 5;
$moftype[$nr] = 3;
$limit[$nr] = 10000000;     
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 15; 
$nr++;
$mof[$nr]  = "Inovace projektu"; 
$mofhelp[$nr]  = "Popište, v čem je váš projekt výjimečný a inovativní, jaká nová řešení jste aplikovali:";
$mofnr[$nr] = 6;
$moftype[$nr] = 3;
$limit[$nr] = 10000000;     
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle;
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 5; 
$nr++;
 
$mof[$nr]  = "Rozvoj projektu"; 
$mofhelp[$nr]  = "Popište, jakým způsobem budete projekt v příštích letech rozvíjet a proč a jak zajistíte jeho dlouhodobou udržitelnost:";
$mofnr[$nr] = 7;
$moftype[$nr] = 3;
$limit[$nr] = 10000000;     
$moffilename[$nr] = 'file'.$nr;
$mofkat[$nr] = $mofmainstyle; 
$mofhod[$nr]= 'man';
$mofqhodmax[$nr]	= 5;
$mofqhodweight[$nr]	= 7.5;      
$nr++;                                 
 ?>