<div id='kroky3'>
<h2><span>3</span>kroky k přihlášení</h2>
<span class='number'>1</span>
<p><a href='<?php echo  getPageLink(10); ?>'>Zaregistrujte se</a>. Heslo vám pak umožní  vyplňovat přihlášky.</p>  
<span class='number'>2</span>
<p>Vyberte si kategorii a vyplňte přihlášku. Přihlášku lze uložit on-line a vracet se  k obsahu později. Lze ji též vyplnit  ve wordu a finální obsah zkopírovat  do on-line verze.  </p>  
<span class='number'>3</span>
<p>Finální verzi přihlášky odešlete  kliknutím na tlačítko Uložení / Odeslání.</p>  
</div>

 