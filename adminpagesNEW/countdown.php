<!-- AP-NAME: Homepage Odpočet -->
<!-- AP-VALU: Zahájení:1; Uzavření:2; Vyhlášení:3 -->

<?php
  if($autovalue == 1) {
  $doceho = "Už jen tolik dní zbývá do zahájení dalšího ročníku ;-)" ;
  }
  elseif($autovalue == 2) {
    $doceho = "Už jen tolik dní zbývá do uzavření soutěže ;-)" ;
  }
  elseif($autovalue == 3){
    $doceho = "Už jen tolik dní zbývá do vyhlášení výsledků ;-)" ;
  }
  else{}

?>

   <div id='countdown' title='<?php echo $doceho; ?>'>
    <span id='numberof' title='<?php echo $doceho; ?>'><?php echo $days = number_format (floor((time() - strtotime($partrow['ppValue'])) / (3600 * 24))); ?></span>
    <span id='days' title='<?php echo $doceho; ?>'>
    <?php
      if($days == -1) { echo "den"; }
      elseif($days <  4) { echo "dní"; }
      else { echo "dny"; }
    ?>
    </span>
    <div title='<?php echo $doceho; ?>'></div>
    <!-- <?php echo strtotime($partrow['ppValue']); ?> | <?php echo time(); ?> -->
</div>