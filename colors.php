@media screen and (max-width: 1200px)
{
    .home #main {
        height: auto;
        width: 100%;
        padding: 0 2%;  
        margin-top: 100px;
    }
    .page #main {
        height: auto;
        width: 100%;
        padding: 0 2%;  
    }
    .page  #main #content {  
        margin-top: 100px;
    }
}
@media screen and (max-width: 900px)
{
    .home #main #content div {  
        width: calc((100% - 40px) /2 );
    }
    .home #main #content {  
        margin: 0em;
    }
}
@media screen and (max-width: 600px)
{
    .home #main #content div {  
        width:  100% ;
        margin:  5px 0;
    } 
}