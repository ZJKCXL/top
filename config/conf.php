<?

/**
 * Define website variables
 */

define("BASE_URL", "http://www.odpovednefirmy.cz/");
define("COOKIE_PATH", "/");
define("COOKIE_DOMAIN", "odpovednefirmy.cz/");
define("LANG_DEFAULT", "cz");
define("APP_NAME", "PRL");
define("APP_CHARSET", "utf-8");
define("CB_SCRIPT_DIR","/clearbox");

$MONTHS["en"]["short"] = array(
	1 => "Jan",
	2 => "Feb",
	3 => "Mar",
	4 => "Apr",
	5 => "May",
	6 => "Jun",
	7 => "Jul",
	8 => "Aug",
	9 => "Sep",
	10 => "Oct",
	11 => "Nov",
	12 => "Dec",
);

$MONTHS["cz"]["short"] = array(
	1 => "Led",
	2 => "Úno",
	3 => "Bře",
	4 => "Dub",
	5 => "Kvě",
	6 => "Čer",
	7 => "Čvn",
	8 => "Srp",
	9 => "Zář",
	10 => "Říj",
	11 => "Lis",
	12 => "Pro",
);

$MONTHS["ru"]["short"] = array(
	1 => "янв",
	2 => "февр",
	3 => "март",
	4 => "апр",
	5 => "май",
	6 => "июнь",
	7 => "июль",
	8 => "авг",
	9 => "сент",
	10 => "окт",
	11 => "ноябрь",
	12 => "дек",
);

$MONTHS["de"]["short"] = array(
	1 => "Jan",
	2 => "Feb",
	3 => "März",
	4 => "Apr",
	5 => "May",
	6 => "Juni",
	7 => "Juli",
	8 => "Aug",
	9 => "Sep",
	10 => "Okt",
	11 => "Nov",
	12 => "Dez",
);

$MONTHS["fr"]["short"] = array(
	1 => "Janv",
	2 => "Févr",
	3 => "Mars",
	4 => "Avril",
	5 => "Mai",
	6 => "Juin",
	7 => "Juil",
	8 => "Août",
	9 => "Sept",
	10 => "Oct",
	11 => "Nov",
	12 => "Déc",
);

?>