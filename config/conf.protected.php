<?

/**
 * Define protected variables
 */
                    
define("BASE_DIR", "/var/www/virtual/odpovednefirmy.cz/htdocs/");
define("LOGS_DIR", "/var/www/virtual/odpovednefirmy.cz/htdocs/logs/");
define("CACHE_DIR", "/var/www/virtual/odpovednefirmy.cz/htdocs/caches/");

 

//Colors 4 CSS files

$extraFont = "'Source Sans Pro'" ;

 function rgb2html($r, $g=-1, $b=-1)
{
    if (is_array($r) && sizeof($r) == 3)
        list($r, $g, $b) = $r;

    $r = intval($r); $g = intval($g);
    $b = intval($b);

    $r = dechex($r<0?0:($r>255?255:$r));
    $g = dechex($g<0?0:($g>255?255:$g));
    $b = dechex($b<0?0:($b>255?255:$b));

    $color = (strlen($r) < 2?'0':'').$r;
    $color .= (strlen($g) < 2?'0':'').$g;
    $color .= (strlen($b) < 2?'0':'').$b;
    return '#'.$color;
}
        
        
 
 
 function html2r($hex){
 list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
 return $r;
 }
 function html2g($hex){
 list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
 return $g;
 }
 function html2b($hex){
 list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
 return $b;
 } 
 
 //na tohle se saha!

 $maincolor = '#426400'; 
 $color2 = '#688d1b'; //puvodni
 $color3 = '#486c00'; 
 $color4 = '#77a320';  
 $colortxt = '#404040';
 $lightcolor = '#f0f9dd';
 $radius = 15;
 $normalfont = "'Roboto', sans-serif;";
?>