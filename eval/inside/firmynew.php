<style>
        .bignebig{
         display: block;
         float: left;
         margin-top: 0px;
         width: auto;
        }
        .bigneg{
         width: 200px;
         margin-top: 4px;
 
        }  
        .lft {
            float: left;
            margin-right: 15px;
            display: inline ;
        }
  .deletecomp {
    display: block;
    float: right;
    background: red;
    color: #fff;
    padding: 5px 15px;
   }  
   .deletecomp:hover{
       color: #fff;
      background: #b53b3b
   }
</style>
<?php
$table_name = "users";
$page_name = "firmy";

if (isset($_REQUEST["newsid"]) && $_REQUEST["newsid"]>0)
{
    $newsid = $_REQUEST["newsid"];
}
else
{
    $newsid = false;
}


if (@$newsid)
{
    $query = "SELECT * FROM ".$table_name." WHERE ID =".$newsid;
   //echo $query;
    $res = mysql_query($query);
    if ($res && @mysql_num_rows($res)>0)
    {
        $row = mysql_fetch_array($res);
    }
}



?>

<h1>Detail společnosti</h1>
 
<form ENCTYPE="multipart/form-data" action="index.php?id=<?php echo $page_name; ?><?php if($newsid>0){echo "&newsid=",$newsid;}?>" method="post" name="noname" id="kontakt">
 
<div class="row">
  <fieldset class="col-lg-6  col-sm-12"> 

  
 
   <div class="input-group mb-3">
     <div class="input-group-text">Přihl. jméno a heslo</div>
        <div class="form-control two-inputs">
        <input class="picwidthVimeo"  type="text"  required  placeholder="šířka" name="nickname"  value="<?php echo $row['nickname']; ?>"/ >
        <input class="picheightVimeo" type="text"  required  placeholder="výška" name="fanpass" value="<?php echo $row['fanpass']; ?>"/ >
    </div>
 </div>
 
 <?php 
    echo bootInput('Název firmy:','required','text',$row['company'],'company',$js); 
    echo bootInput('Sídlo firmy:','required','text',$row['companyaddr'],'companyaddr',$js);  
    echo bootInput('IČO:','required','text',$row['companynr'],'companynr',$js);     
    echo bootInput('WWW:','required','text',$row['companywww'],'companywww',$js);     
    echo bootInput('Kontaktní osoba:','required','text',$row['contact'],'contact',$js);     
    echo bootInput('Funkce:','required','text',$row['function'],'function',$js); 
    echo bootInput('E-mail:','required','text',$row['fanemail'],'fanemail',$js);  
    echo bootInput('Mobil:','required','text',$row['fanphone2'],'fanphone2',$js); 
?>
 
 <strong>Popis činnosti:</strong> 
   <textarea name="companytxt"><?php echo $row['companytxt']; ?></textarea>


   </fieldset>
   <fieldset class="col-lg-6  col-sm-12"> 


  <div class="input-group mb-3">
  <div class="input-group-prepend">
  <label class="input-group-text" for="inputGroupSelect01">Povaha firmy:</label>
  </div>
  <div class="form-control ">
  <select required name="stylecompany">
   <option value="">--nevybráno--</option>
   <option value="N" <?php if($row['stylecompany'] == 'N'){ echo " selected "; } ?> >Nevýrobní</option>
   <option value="V" <?php if($row['stylecompany'] == 'V'){ echo " selected "; } ?>>Výrobní</option>
   </select>
</div>
</div>

  <div class="input-group mb-3">
  <div class="input-group-prepend">
  <label class="input-group-text" for="inputGroupSelect01">Působení firmy:</label>
  </div>
  <div class="form-control ">
  <select required name="homecompany">
   <option value="">--nevybráno--</option>
   <option value="H" <?php if($row['homecompany'] == 'H'){ echo " selected "; } ?> >Působí pouze v ČR na regionálním trhu</option>
   <option value="HH" <?php if($row['homecompany'] == 'HH'){ echo " selected "; } ?> >Působí pouze v ČR na celostátním trhu </option>
   <option value="HW" <?php if($row['homecompany'] == 'HW'){ echo " selected "; } ?> >Působí ve více zemích s centrálou v ČR</option>
   <option value="W" <?php if($row['homecompany'] == 'W'){ echo " selected "; } ?>> Mezinárodní (firma je součástí nadnárodního řetězce)</option>
   </select>
</div>
</div>

  <div class="input-group mb-3">
  <div class="input-group-prepend">
  <label class="input-group-text" for="inputGroupSelect01">Velikost firmy:</label>
  </div>
  <div class="form-control ">
  <select required name="bigcompany">
   <option value="">--nevybráno--</option>
   <option value="1" <?php if($row['realbig'] == '1'){ echo " selected "; } ?> >Velká firma</option>
   <option value="0" <?php if($row['realbig'] == '0'){ echo " selected "; } ?>>Malá firma</option>  
   </select>
</div>
</div>
  
 
   
   <strong class='lft'>Počet zaměstnanců:</strong> 
   <?php 
    if($row['bigcompany2'] == 'P') { echo "<span class='bignebig'>do 10</span>"; }
    elseif($row['bigcompany2'] == 'M') { echo "<span class='bignebig'>do 50</span>"; }
    elseif($row['bigcompany2'] == 'S') { echo "<span class='bignebig'>do 249</span>"; }
    elseif($row['bigcompany2'] == 'V') { echo "<span class='bignebig'>nad 250 [tedy spadá do velké]</span>"; }
    else{ echo "neuvedeno"; }
   ?>
   <br/>
   <strong class='lft'>Roční obrat</strong> 
   <?php 
    if($row['bigcompany3'] == 'P') { echo "<span class='bignebig'>do 2 mil. EUR</span>"; }
    elseif($row['bigcompany3'] == 'M') { echo "<span class='bignebig'>do 10 mil. EUR</span>"; }
    elseif($row['bigcompany3'] == 'S') { echo "<span class='bignebig'>do 50 mil. EUR</span>"; }
    elseif($row['bigcompany3'] == 'V') { echo "<span class='bignebig'>nad 50 mil. EUR [tedy spadá do velké]</span>"; }
    else{ echo "neuvedeno"; }
   ?>   
   <br/>
   <strong class='lft'>Bilanční suma roční rozvahy</strong>  
   <?php 
    if($row['bigcompany3'] == 'P') { echo "<span class='bignebig'>do 2 mil. EUR</span>"; }
    elseif($row['bigcompany3'] == 'M') { echo "<span class='bignebig'>do 10 mil. EUR</span>"; }
    elseif($row['bigcompany3'] == 'S') { echo "<span class='bignebig'>do 43 mil. EUR</span>"; }
    elseif($row['bigcompany3'] == 'V') { echo "<span class='bignebig'>nad 43 mil EUR [tedy spadá do velké]</span>"; }
    else{ echo "neuvedeno"; }
   ?>      
  <br/><br/>
  <div class="input-group mb-3">
  <div class="input-group-prepend">
  <label class="input-group-text" for="inputGroupSelect01">Obor působení:</label>
  </div>
  <div class="form-control ">
  <select name="sektor" required>
   <option value="">--nevybráno--</option>
   
<option <?php if ($row[sektor]==1){ echo " selected "; } ?> value="1">Těžba nerostných surovin  </option>
 <option <?php if ($row[sektor]==2){ echo " selected "; } ?> value="2">Energetika</option> 
 <option <?php if ($row[sektor]==3){ echo " selected "; } ?> value="3">Těžký průmysl</option>
 <option <?php if ($row[sektor]==4){ echo " selected "; } ?> value="4">Doprava a logistika</option>
 <option <?php if ($row[sektor]==5){ echo " selected "; } ?> value="5">Stavebnictví</option>
 <option <?php if ($row[sektor]==6){ echo " selected "; } ?> value="6">Reality</option>
 <option <?php if ($row[sektor]==7){ echo " selected "; } ?> value="7">Cestovní ruch</option>
 <option <?php if ($row[sektor]==8){ echo " selected "; } ?> value="8">Spotřební průmysl</option>
 <option <?php if ($row[sektor]==9){ echo " selected "; } ?> value="9">Farmacie a biologie</option>
 <option <?php if ($row[sektor]==10){ echo " selected "; } ?> value="10">Rychloobrátkové spotřební zboží</option>
 <option <?php if ($row[sektor]==12){ echo " selected "; } ?> value="12">Obchod</option>
 <option <?php if ($row[sektor]==13){ echo " selected "; } ?> value="13">Technologie a IT</option>
 <option <?php if ($row[sektor]==14){ echo " selected "; } ?> value="14">Telekomunikační a poštovní služby</option> 
 <option <?php if ($row[sektor]==15){ echo " selected "; } ?> value="15">Bankovnictví a finanční služby</option> 
 <option <?php if ($row[sektor]==16){ echo " selected "; } ?> value="16">Poradenské služby</option> 
 <option <?php if ($row[sektor]==17){ echo " selected "; } ?> value="17">Vzdělávání</option>
 <option <?php if ($row[sektor]==18){ echo " selected "; } ?> value="18">Média</option>
 <option <?php if ($row[sektor]==19){ echo " selected "; } ?> value="19">Jiný obor</option>  
   </select>
</div>
</div>

   <?php
   echo bootInput('Jiný obor:','','text',$row['othersektor'],'othersektor',$js); 
   echo bootInput('Podíl státu:','','text',$row['companyown'],'companyown',$js); 
   ?>

 <p>&nbsp;</p>
 <p>&nbsp;</p>

 <?php 
    echo bootInput('Uhradit:',' ','text',$row['topay'],'topay',$js);  
    echo bootInput('Placeno:',' ','text',$row['paid'],'paid',$js); 
?>
 
  <div style='clear: left;'></div>  
 </fieldset>

</div>



<a class='deletecomp' onClick="return confirm('Skutečně chcete vymazat firmu z databáze?')" href='/admin/index.php?id=firmy&deletemecomp=<?php echo $newsid; ?>'>Smazat Firmu</a>

<input type="submit" value="Uložit změny" name="send" class="btn btn-primary  " /> 
                                      
</form>
