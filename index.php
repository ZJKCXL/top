<?php
header("content-type: text/html; charset=utf-8");
$page = $_REQUEST['page'];
$section = $_REQUEST['section'];
$note = $_REQUEST['note'];
$blabla = $_REQUEST['blabla'];
include_once("./lib/globals19.php");
include ".". Globals::$GLOBAL_SQL_FILE;
include_once("./lib/mysqlPHP.php");
include "./lib/pageswitcher.php";
include "./lib/lib.php";
include "./lib/seo.php";
include "./lib/pager.php";
include("./lib/top_user_functions.php");
include("./lib/StringUtils.php");
$time = time();
//loginOLD
if((isset($_POST['oldnickname'])) && (isset($_POST['oldfanpass'])))  
{
    if(old_user_login(trim($_POST['oldnickname']), trim($_POST['oldfanpass']))) { 
        header("Location: http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    }
}
//loginNORMAL
if((isset($_POST['nickname'])) && (isset($_POST['fanpass'])))  
{
    if(user_login(trim($_POST['nickname']), trim($_POST['fanpass']))) { 
        header("Location: http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    }
}
//logout
if((isset($_GET['please'])) && ($_GET['please'] == 'deleteme')) {
  user_logout();
  header("Location: /index.php");
}  
 
?><!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title><?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'title'); ?></title>
    <meta name='description' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'desc'); ?>' />
    <meta name='keywords' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'keywords'); ?>' />
    <?php
    //echo strlen(pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'fbimg'));
    if(strlen(pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'fbimg'))>0){
    ?><meta property="og:image" content="<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'fbimg'); ?>" />
    <?php
    }
    ?><meta property="og:title" content="<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid, 'title'); ?>"/>
    <meta property="og:url" content="<?php  echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"/>
    <meta property='og:description' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid, 'desc'); ?>' />
    <meta property="og:type" content="website" />
    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/manifest.json">
    <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#000000">
    <link rel="stylesheet" href="<?php echo $tempdir; ?>/font-awesome/css/font-awesome.min.css">    
    <link href="<?php echo $tempdir; ?>/css/parallax.css?ver=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $tempdir; ?>/css/word.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $tempdir; ?>/css/layout.css?ver=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $tempdir; ?>/css/seno.css?ver=<?php echo time(); ?>" rel="stylesheet" type="text/css" />    
    <?php echo pageWTF ($finalpage,$page,$finalsection,$finalID,$finalnewsid,'css'); ?>
<meta name="robots" content="index,follow" />
    <script src="<?php echo $tempdir; ?>/js/jquery/jquery.js" type="text/javascript"></script>
    <script src="<?php echo $tempdir; ?>/js/jquery.sticky.js" type="text/javascript"></script>        
    <script src="<?php echo $tempdir; ?>/js/scripts.js?ver=<?php echo time(); ?>"  xdefer  ></script>
    <script src="<?php echo $tempdir; ?>/js/clearbox.js" type="text/javascript" charset="utf-8"></script>  
     

    
   <link rel="stylesheet" type="text/css"  href="<?php echo $tempdir; ?>/fonts/style.css" media="all" />    
<?php
    if( $ishome == 1){
    ?>
        <link href="<?php echo $tempdir; ?>/css/responseHome.css" rel="stylesheet" type="text/css" media="all"  />
    <?php        
    }else{
    ?>
        <link href="<?php echo $tempdir; ?>/css/responsePage.css" rel="stylesheet" type="text/css" media="all" />
    <?php
    }
    ?>
    <link rel="stylesheet" href="<?php echo $tempdir; ?>/css/print.css" type="text/css" media="print" />
    </head>
<body <?php if( $ishome == 1){ echo "class='homepage'";  }elseif($finalpage) { echo "class='".$page." ".$finalpage."  ".$finalsection."'";  }else{} ?>  >
 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44694781-5', 'auto');
  ga('send', 'pageview');

</script>
<span id='startmenu' title='MENU'></span>
<ul class="mini-menu" id="mini-menu"> 
                <?php
                $type = 0;
                include ('./inside/menu.php');
                $type = 3;
                include ('./inside/menu.php');                
                ?>
                </ul>
    <nav>
        <div>
            <?php
            if( $ishome == 1){
            ?>
<h1 id='logo'><a href='<?php echo $tempdir; ?>/'><span><?php echo $webname; ?></span></a></h1>
                <?php
            }else{
                ?>
<div id='logo'><a href='<?php echo $tempdir; ?>/'><span><?php echo $webname; ?></span></a></div>
            <?php
            }
            ?>
<div id='logotop'><a href='<?php echo $tempdir; ?>/cena-top/cena-top.html'><span>Cena TOP 2017</span></a></div>
<div id='homeclaim'>
<?php
                if( $ishome == 1){
                $partrow['ppValue'] = 1;
                include ('./adminpages/headerhomebox.php');
                }
                ?>
</div>
            <?php
            if( $ishome == 1){
                ?>
<ul id="menu1">
<?php
   $type = 1;
   include ('./inside/menu.php');
?>
</ul>
     <?php
            }
            ?>           
 

    </div>

 

    </nav>
    <div id='wrapper'>
    <div id='main'>
    <?php
    include ('./inside/content.php');
    ?>
    </div><!-- div main end -->
    <div id='partners'>
                    <?php
                     include ('./inside/loga.php');
                    ?>
    </div>
    <footer>
        <div>
         <div class='tretina'>
                    <h4><a href="/kontakty.html" title="kontakty">kontakt</a></h4>
                    <?php
                    include ('./adminpages/microdata_footer.php');
                    ?>
                    <a class='mainclr' href='http://www.byznysprospolecnost.cz'>Web BPS</a>
        </div>   
         <div class='tretina'>
                    <h4>další projekty BPS</h4>
                    <?php
                    include ('./inside/menudown.php');
                    ?>
        </div>
        
<div id="social " class='tretina'> 
   <a class="social fa fa-facebook-official" href="https://www.facebook.com/byznysprospolecnost" alt="Facebook"><span>Facebook</span></a>
     <a class="social fa fa-youtube-square" href="https://www.youtube.com/channel/UCZumBFeVi9HFaYxSh-9aldQ" alt="YouTube"><span>YouTube</span></a>
    <a class="social fa fa-twitter-square" href="https://twitter.com/odpovednefirmy" alt="Twitter"><span>Twitter</span></a>
    <a class="social fa fa-linkedin-square" href="https://www.linkedin.com/company-beta/2359122" alt="LinkdeIN"><span>LinkdeIN</span></a>
  </div>
        
             <div class='mainclr'> </div>
        </div>
    </footer>
    </div>
 
</body>
</html>