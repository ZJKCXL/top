var prevent = {};
prevent.init = function ()
{
	var re = new RegExp('#$');

	var anchors = document.getElementsByTagName('a');
	for (var i = 0; i < anchors.length; i++)
	{
		if (anchors[i].href.match(re))
		{
			event_handler.remove(anchors[i], 'click', prevent.prevent);
			event_handler.add(anchors[i], 'click', prevent.prevent);
		}
	}
};
prevent.prevent = function (event)
{
	event = event_handler.fix(event);

	if (event.preventDefault)
	{
		event.preventDefault();
	}
};