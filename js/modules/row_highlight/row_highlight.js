var row_highlight = {};
row_highlight.init = function ()
{
	var elements = document.getElementsByTagName('table');
	for (var i = 0; i < elements.length; i++)
	{
		if (class_handler.has(elements[i], 'row_highlight'))
		{
			var tmp_tbody = elements[i].getElementsByTagName('tbody');
			for (var j = 0; j < tmp_tbody.length; j++)
			{
				var tmp_row = tmp_tbody[j].getElementsByTagName('tr');
				for (var k = 0; k < tmp_row.length; k++)
				{
					event_handler.add(tmp_row[k], 'mouseover', row_highlight.mouseover);
					event_handler.add(tmp_row[k], 'mouseout', row_highlight.mouseout);
				}
			}
		}
	}
}
row_highlight.mouseover = function (event)
{
	var event = event_handler.fix(event);
	var element = event.target;

	/*var td_element = event.target;
	while (td_element.tagName.toLowerCase() != 'td' && td_element.tagName.toLowerCase() != 'tr')
	{
		td_element = td_element.parentNode;
	}*/

	while (element.tagName.toLowerCase() != 'tr')
	{
		element = element.parentNode;
	}

	/*var order = 0;
	var children = element.childNodes;
	for (var i = 0; i < children.length; i++)
	{
		if (children[i] == td_element)
		{
			break;
		}

		if (children[i].nodeType == 1)
		{
			order++;
		}
	}*/

	var cells = element.getElementsByTagName('td');
	for (var i = 0; i < cells.length; i++)
	{
		if (!class_handler.has(cells[i], 'row_highlight_highlight'))
		{
			class_handler.remove(cells[i], 'row_highlight_highlight');
			class_handler.add(cells[i], 'row_highlight_highlight');
		}
	}

	var cells = element.getElementsByTagName('th');
	for (var i = 0; i < cells.length; i++)
	{
		if (!class_handler.has(cells[i], 'row_highlight_highlight'))
		{
			class_handler.remove(cells[i], 'row_highlight_highlight');
			class_handler.add(cells[i], 'row_highlight_highlight');
		}
	}

	/*var rows = element.parentNode.getElementsByTagName('tr');
	for (var i = 0; i < rows.length; i++)
	{
		var tmp_count = 0;

		var cells = rows[i].childNodes;
		for (var j = 0; j < cells.length; j++)
		{
			if (cells[j].nodeType == 1)
			{
				if (tmp_count == order)
				{
					class_handler.remove(cells[j], 'row_highlight_highlight_vertical');
					class_handler.add(cells[j], 'row_highlight_highlight_vertical');
				}

				tmp_count++;
			}
		}
	}*/
};
row_highlight.mouseout = function (event)
{
	var event = event_handler.fix(event);
	var element = event.target;

	/*var td_element = event.target;
	while (td_element.tagName.toLowerCase() != 'td' && td_element.tagName.toLowerCase() != 'tr')
	{
		td_element = td_element.parentNode;
	}*/

	while (element.tagName.toLowerCase() != 'tr')
	{
		element = element.parentNode;
	}

	/*var order = 0;
	var children = element.childNodes;
	for (var i = 0; i < children.length; i++)
	{
		if (children[i] == td_element)
		{
			break;
		}

		if (children[i].nodeType == 1)
		{
			order++;
		}
	}*/

	var cells = element.getElementsByTagName('td');
	for (var i = 0; i < cells.length; i++)
	{
		if (class_handler.has(cells[i], 'row_highlight_highlight'))
		{
			class_handler.remove(cells[i], 'row_highlight_highlight_vertical');
			class_handler.remove(cells[i], 'row_highlight_highlight');
		}
	}

	var cells = element.getElementsByTagName('th');
	for (var i = 0; i < cells.length; i++)
	{
		if (class_handler.has(cells[i], 'row_highlight_highlight'))
		{
			class_handler.remove(cells[i], 'row_highlight_highlight_vertical');
			class_handler.remove(cells[i], 'row_highlight_highlight');
		}
	}

	/*var rows = element.parentNode.getElementsByTagName('tr');
	for (var i = 0; i < rows.length; i++)
	{
		var tmp_count = 0;

		var cells = rows[i].childNodes;
		for (var j = 0; j < cells.length; j++)
		{
			if (cells[j].nodeType == 1)
			{
				if (tmp_count == order)
				{
					class_handler.remove(cells[j], 'row_highlight_highlight_vertical');
				}

				tmp_count++;
			}
		}
	}*/
};