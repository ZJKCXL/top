<?

/////////
//EXAMPLE
/////////

$dir = substr($_SERVER['SCRIPT_FILENAME'], strlen($_SERVER['DOCUMENT_ROOT']));
$dir = substr($dir, 0, strpos($dir, "/", 1)+1);

include_once($_SERVER['DOCUMENT_ROOT'].$dir."config/conf.protected.php");
include_once($_SERVER['DOCUMENT_ROOT'].$dir."config/conf.php");

include_once("../../../config/conf.php");
include_once(BASE_DIR."sql/db.php");

header("Content-type: text/xml; charset=iso-8859-2");

echo "<?xml version=\"1.0\" encoding=\"iso-8859-2\"?>";
echo "<response>";

if ($_REQUEST["type"] == "person")
{
	if (strlen(trim($_REQUEST["phrase"])) > 0)
	{
		$inquery = " AND (LOWER(personName) LIKE '%".mysql_real_escape_string(mb_strtolower($_REQUEST["phrase"]),$link)."%' ";
		$inquery  .= " Or LOWER(personAsciName) LIKE '%".mysql_real_escape_string(mb_strtolower($_REQUEST["phrase"]),$link)."%' ";
		$inquery  .= " Or LOWER(otherName) LIKE '%".mysql_real_escape_string(mb_strtolower($_REQUEST["phrase"]),$link)."%' ";
		$inquery  .= " Or LOWER(otherAsciName) LIKE '%".mysql_real_escape_string(mb_strtolower($_REQUEST["phrase"]),$link)."%')";
	}

	$query = "SELECT *, tbl_app_countries.ID as CID";
	$query .= ", tbl_app_people.ID as PID";
	$query .= " FROM (tbl_app_people)";
	$query .= " LEFT JOIN tbl_app_countries ON (tbl_app_countries.ID = tbl_app_people.personCountry)";
	$query .= " LEFT JOIN tbl_app_othernames ON (tbl_app_othernames.otherID = tbl_app_people.ID)";
	$query .= " WHERE 1";
	$query .= $inquery;
	$query .= " GROUP BY PID";
	$query .= " ORDER BY tbl_app_people.personName";
	if ($_REQUEST["limit"] > 0)
	{
		$query .= " LIMIT ".$_REQUEST["limit"];
	}

	$res = @mysql_query($query);
	while ($row = @mysql_fetch_array($res))
	{
		$gender_img = $row["personGender"] == 1 ? BASE_URL."file_type_images/man.gif" : BASE_URL."file_type_images/woman.gif";
		$flag_img = BASE_URL."vlajky/small/".strtolower($row["countryCode"]).".png";
		$famous_img = BASE_URL."file_type_images/star.gif";

		echo "<item>";
		//id prvku
		echo "<id>".$row["PID"]."</id>";
		//plaintext jednoduchy nazev - napr. jen jmeno (to, co se vyplni po vybrani do inputu)
		echo "<display>".$row["personName"]."</display>";
		//html verze prvku zobrazovana v naseptavaci
		echo "<html_display>";
		//CDATA start
		//	- html verze MUSI byt ohranicena CDATA
		echo "<![CDATA[";
		//html start
		echo "<img src=\"".$flag_img."\" />";
		echo " <img src=\"".$gender_img."\" />";
		echo " ".$row["personName"];
		if ($row["personFamous"] == 1)
		{
			echo " <img src=\"".$famous_img."\" />";
		}
		echo "<em class=\"float_right\">".$row["personTown"]."</em>";
		//html end
		echo "]]>";
		//CDATA end
		echo "</html_display>";
		//id elementu, kam se ma ulozit vybrane id -> tj. mam osobu s ID 12 - vyberu ji v naseptavaci a do inputu
		//s id = suggest_person_id se da jako value 12
		echo "<value_element_id>suggest_person_id</value_element_id>";
		echo "</item>";
	}
}

echo "</response>";