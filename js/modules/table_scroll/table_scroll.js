var table_scroll = {};
table_scroll.offsetTop = 0;
table_scroll.headers = new Array();
table_scroll.clones = new Array();
table_scroll.init = function ()
{
	event_handler.add(window, 'scroll', table_scroll.scroll);

	var all_tables = document.getElementsByTagName('table');
	for (var i = 0; i < all_tables.length; i++)
	{
		if (class_handler.has(all_tables[i], 'table_scroll') && !class_handler.has(all_tables[i], 'cloned'))
		{
			table_scroll.headers[table_scroll.headers.length] = all_tables[i].getElementsByTagName('thead')[0];
			if (!class_handler.has(all_tables[i], 'cloned'))
			{
				class_handler.add(all_tables[i], 'cloned');
			}

			for (var j = 0; j < table_scroll.headers.length; j++)
			{
				table_scroll.clones[j] = table_scroll.headers[j].cloneNode(true);
				table_scroll.clones[j].style.width = (table_scroll.headers[j].offsetWidth+2)+'px';
				table_scroll.clones[j].style.display = 'none';
				class_handler.add(table_scroll.clones[j], 'table_scroll_fixed_header');

				table_scroll.headers[j].parentNode.insertBefore(table_scroll.clones[j], table_scroll.headers[j]);
			}
		}
	}
};
table_scroll.scroll = function (event)
{
	var event = event_handler.fix(event);

	var scroll_y = position.getScrollY(event);

	var scroll_x = position.getScrollX(event);

	for (var i = 0; i < table_scroll.headers.length; i++)
	{
		var header_th_set = table_scroll.headers[i].getElementsByTagName('th');
		var clone_th_set = table_scroll.clones[i].getElementsByTagName('th');

		table_scroll.clones[i].style.left = (position.getOffsetX(table_scroll.headers[i])-scroll_x)+'px';

		for (var j = 0; j < header_th_set.length; j++)
		{
			clone_th_set[j].style.padding = '';
			clone_th_set[j].style.padding = header_th_set[j].style.padding;
			clone_th_set[j].style.width = '';
			clone_th_set[j].style.width = (header_th_set[j].offsetWidth-header_th_set[j].style.padding)+'px';
		}

		var pos_y = position.getOffsetY(table_scroll.headers[i])-table_scroll.offsetTop;

		if (scroll_y > pos_y)
		{
			table_scroll.clones[i].style.display = 'block';
			table_scroll.clones[i].style.position = 'fixed';
			table_scroll.clones[i].style.top = table_scroll.offsetTop+'px';
		}
		else
		{
			table_scroll.clones[i].style.display = 'none';
		}
	}
};