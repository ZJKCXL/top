// JavaScript Document
var tempdir = '';

window.onresize = settle;
 
$(window).scroll(function() {
  if($(window).scrollTop() + $(window).height() > $(document).height() - 300) {
    $("#immerSend").removeClass('Normal')
  }
  else{
    $("#immerSend").addClass('Normal')
  }
  
});

$(document).ready(function () {
 // slideShow();
  settle(); 
 
  $("input").change(function () 
            {
              flagChanges();
            });
 $("textarea").change(function () 
            {
              flagChanges();
            }); 

 $("input:checkbox").not( document.getElementById( "closeMe" ) ).change(function () 
 {
  if ( $('#closeMe').prop( "checked" ) ){ 
 
         var   myclass = $(this).attr("class");
          
          if ($('input.'+ myclass).is(':checked')) {
          $( "#" + myclass + " input").prop('required', false); 
          
        }
        else{
          $( "#" + myclass + " input").prop('required', true);  
        }
     
    }
 }); 


 checkRequired();
 checkBoxStartRequired();

  if(document.getElementById('singlemenu0')){
  menu();
  }
  startmenu();
   $("#singlemenu3").sticky({topSpacing:0});
  $("#singlemenu0").sticky({topSpacing: 0}); 
   
   if(document.getElementById('ctr')){
   document.getElementById('ctr').value = "none";
  }

    

  
   //	$("#content-right").sticky({topSpacing: 48});
    
 

  if (document.getElementById('dotazform')||document.getElementById('cnt2')||document.getElementById('loginform')) {

        document.querySelector("form")
            .addEventListener("invalid", function (event) {
                event.preventDefault();
            }, true);


        function  replaceValidationUI(form) {
            // Suppress the default bubbles
            form.addEventListener("invalid", function (event) {
                event.preventDefault();
            }, true);

            // Support Safari, iOS Safari, and the Android browser each of which do not prevent
            // form submissions by default
            form.addEventListener("submit", function (event) {
                if (!this.checkValidity()) {
                    event.preventDefault();
                }
            });

            var submitButton = form.querySelector("button:not([type=button]), input[type=submit]");
            submitButton.addEventListener("click", function (event) {
                var invalidFields = form.querySelectorAll(":invalid"),
                    errorMessages = form.querySelectorAll(".error-message"),
                    parent;

                // Remove any existing messages
                for (var i = 0; i < errorMessages.length; i++) {
                  errorMessages[i].parentNode.removeChild(errorMessages[i]);
                }

                for (var i = 0; i < invalidFields.length; i++) {
                    parent = invalidFields[i].parentNode;
                    parent.insertAdjacentHTML("beforeend", "<div class='error-message'>" +
                        invalidFields[i].validationMessage +
                        "</div>");
                    $('.error-message').animate({ opacity: '1' }, 500);
                }

                // If there are errors, give focus to the first invalid field
                if (invalidFields.length > 0) {
                     
                    invalidFields[1].focus();
                }
            });
        }

        // Replace the validation UI for all forms
        var forms = document.querySelectorAll("form");
        for (var i = 0; i < forms.length; i++) {
           replaceValidationUI(forms[i]);
        }

    }
 

});



function menu() {
  var nav = document.getElementById('singlemenu0');
  var as = nav.getElementsByTagName('LI');
  for (var i = 0; i <  as.length; i++) {
      var folder = as[i].getElementsByTagName('UL');
      if (folder.length > 0) {
          as[i].number = i;
          as[i].onclick = test;
       }
  }
  }


function slideExcept0() {
  var nav = document.getElementById('singlemenu0');
  var as = nav.getElementsByTagName('LI');
  var folder = as.getElementsByTagName('UL');
  alert(folder.length);
  for (var i = 0; i <  folder.length; i++) {
        folder[i].style.display = 'none';
  }
}


function test(){
 
 
  var nav = document.getElementById('singlemenu0');
  var as = nav.getElementsByTagName('LI');



  for (var x = 0; x <  as.length; x++) {
  if(x != this.number ) {
  var alfolders = as[x].getElementsByTagName('UL');
  for (var i = 0; i <  alfolders.length; i++) {
   
  alfolders[i].style.display = 'none';
   }
  }
  }

  var folder = as[this.number].getElementsByTagName('UL');
  if(folder[0].style.display != 'block'){
  folder[0].style.display = 'block'; 
  }
  else{
  folder[0].style.display = 'none'; 
  }



}

function slideShow() {
  if ($("#slideshow").length <= 0) {
    
    return;
  }
  if ($(".slide").length <= 1) {
    
    return;
  }



  //$('#slideshow div').css({opacity: 0.0; right: -10000px});	
  $('#slideshow .slide').css({ left: -10000 + 'px' });
  $('#slideshow div:first').css({ left: 0 + 'px' });
  $('#slideshow div:first').css({ opacity: 1.0 });
  $('#slideshow .caption').css({ opacity: 1 });
  $('#slideshowmover').click(gallery);
  $('#slideshowleftmover').click(galleryback);
  intervalID = setInterval('gallery()', 10000);

}

function gallery() {

   var mycap =  document.getElementById('slideshow'); 
   var newheight = mycap.offsetHeight  ;
   var newwidth = mycap.offsetWidth  ;   
  var current = ($('#slideshow div.show') ? $('#slideshow div.show') : $('#slideshow div:first'));
  var next = ((current.next().length) ? ((current.next().hasClass('non')) ? $('#slideshow div:first') : current.next()) : $('#slideshow div:first'));
	// var description = current.next('.description') ;	           
	next.css({left: newwidth})
	next.css({opacity: 0})
  .addClass('show')
	next.animate({left: '0px'}, 1)	  
  next.animate({opacity: 1}, 2000)
  current.animate({opacity: 0}, 2000)	
 	current.animate({left: -newwidth}, 1)	
	.removeClass('show');
  clearInterval(intervalID);
  intervalID = setInterval('gallery()', 10000);
  if ($('#my1').attr("class") == 'slide first show') { $('#myem1').css({ background: '#008dd2' }); } else { $('#myem1').css({ background: '#fff' }); }
  for (i = 2; i < ($("#slideshow div").length + 1); i++) {
    var my = '#my' + i;
    var myem = '#myem' + i;
    if ($(my).attr("class") == 'slide show') { $(myem).css({ background: '#008dd2' }); } else { $(myem).css({ background: '#fff' }); }
  }
}

function galleryback() {
  var mycap = document.getElementById('slideshow');
  var newheight = mycap.offsetHeight;
  var newwidth = mycap.offsetWidth;
  var current = ($('#slideshow div.show') ? $('#slideshow div.show') : $('#slideshow div:last'));
  var prev = ((current.prev().length) ? ((current.prev().hasClass('non')) ? $('#slideshow div:last') : current.prev()) : $('#slideshow div:last'));
  //var caption = next.attr('rel');	           
  prev.css({ left: -newwidth })
  /*
  prev.css({ opacity: 1 })
    .addClass('show')
  prev.animate({ left: '0px' }, 500)
  current.animate({ left: newwidth }, 500)
    .removeClass('show');
  */
  prev.css({opacity: 0})
  .addClass('show')
	prev.animate({left: '0px'}, 1)	  
  prev.animate({opacity: 1}, 2000)
  current.animate({opacity: 0}, 2000)	
 	current.animate({left: newwidth}, 500)	
	.removeClass('show');
  clearInterval(intervalID);
  intervalID = setInterval('gallery()', 10000);
  if ($('#my1').attr("class") == 'slide first show') { $('#myem1').css({ background: '#008dd2' }); } else { $('#myem1').css({ background: '#fff' }); }
  for (i = 2; i < ($("#slideshow div").length + 1); i++) {
    var my = '#my' + i;
    var myem = '#myem' + i;
    if ($(my).attr("class") == 'slide show') { $(myem).css({ background: '#008dd2' }); } else { $(myem).css({ background: '#fff' }); }
  }
}



function test2(nr, direction) {
  
  

  if (nr >= 0)
  { this.number = nr; }
  else {
    var x = this.number;
    slideExcept0(this.number);
  }
  var nav = document.getElementById('menu');
  var as = nav.getElementsByTagName('a');
  var spans = as[this.number].parentNode.getElementsByTagName('SPAN');
  var aheights = spans[0].getElementsByTagName('a');

  //pokud v html receno, ze ma byt zobrazen
  if (spans[0].className == "opened") {
    vyska = (aheights.length * 2.25);
    if (spans[0].style.height == "") { spans[0].style.height = vyska + "em"; }
    spans[0].style.display = 'block';
    spans[0].style.overflow = 'hidden';


    //pokud kliknuto na uz zobrazeny - zaviram
    if (parseFloat(spans[0].style.height) > 0.2) {

      spans[0].style.height = parseInt(spans[0].style.height) - 1 + "em";
      nr = this.number;
      direction = 0;
      setTimeout("test2(" + nr + "," + direction + ")", 10);
    }
    else {
      spans[0].style.display = 'none';
      spans[0].style.height = 0;
      spans[0].className = "none";

    }


  }

  // pokud neni ovlivnen html
  else {

    if (spans[0].style.display == "") { spans[0].style.display = "none"; }
    if (spans[0].style.height == "") { spans[0].style.height = 0 + "em"; }


    //pokud je zavreny - oteviram
    if ((spans[0].style.display == "none") || (direction == 1)) {

      spans[0].style.display = 'block';
      spans[0].style.overflow = 'hidden';
      as[this.number].className = 'click';
      var vyska = (aheights.length * 2.25);

 

      // neni otevren uplne

      if (parseFloat(spans[0].style.height) < vyska) {

        spans[0].style.height = (parseFloat(spans[0].style.height) + 1) + "em";
        nr = this.number;
        direction = 1;
        setTimeout("test2(" + nr + "," + direction + ")", 10);

      }
      // uz je otevrenej dost  
      else {
        spans[0].style.height = vyska + "em";
      }
    }

    //pokud je otevreny - zaviram
    else {

      if (parseInt(spans[0].style.height) > 0) {
        var ems = spans[0].getElementsByTagName('em');

        for (var e = 0; e < ems.length; e++) {
          ems[e].style.display = 'none';
        }
        spans[0].style.height = parseFloat(spans[0].style.height) - 1 + "em";
        as[this.number].className = 'noclick';
        nr = this.number;
        direction = 0;
        setTimeout("test2(" + nr + "," + direction + ")", 10);
      }
      else {

        // uplne schovam
        spans[0].style.display = 'none';
        spans[0].style.height = 0;
        as[this.number].className = 'noclick';
      }
    }
  }
 
  return false;

}



function slideExcept0(x) {
  var nav = document.getElementById('singlemenu0');
  var as = nav.getElementsByTagName('LI');
  if(folder[0].style.display == 'none'){
  folder[0].style.display = 'block';
}
else{
 folder[0].style.display = 'none'; 
}
}


function startmenu() {
   
  if (document.getElementById('startmenu')) {
    var startmenu = document.getElementById('startmenu');
    startmenu.onclick = showRespMenu;
  }
}
function showRespMenu() {

  if ($('#mini-menu').css("left") ==  0 + 'px') {
    $('#mini-menu').animate({  left:  -200 + '%'    }, 500);
  }
  else {
    $('#mini-menu').animate({ left: 0   }, 500);
  }

}
 

function checkRequired() {
  if(document.getElementById("closeMe")){
   $('#closeMe').change(toggleRequired);
  }

}

function checkBoxRequired(){
 

}


function checkBoxStartRequired()  {
  
  var inputs = document.getElementsByTagName('input');
  for(var i in inputs) {
      if(inputs[i].type === 'checkbox') {
        if (inputs[i].checked == true) {
           var myclass = inputs[i].getAttribute("class") ;
           $( "#" + myclass + " input").prop('required', false); 
           $( "#" + myclass + " input").removeAttr('sometimesrequired'); 
        }
      }
  }
 

}


function toggleRequired(){
 
  if ( $('#closeMe').prop( "checked" ) ){ 
    
    $("[sometimesrequired]").prop('required', true);
    $("#contactsubmit").val('Uzavřít přihlášku' );
    }
    else{
      $("[sometimesrequired]").prop('required', false);  
      $("#contactsubmit").val('Uložit k dalšímu zpracování' );
     
    }

}

function processRequest()
{
 
	if (httpRequest.readyState == 4)
	{
		if(httpRequest.status == 200)
		{
			var semka = document.getElementById("mytype");
      
			semka.innerHTML = httpRequest.responseText;
      checkForm.init();

   
      
		}
		else
		{
			alert("Chyba pri nacitani stanky"+ httpRequest.status +":"+ httpRequest.statusText);
		}
	}
}



function  settle(){
 
  var hasVScroll = document.body.scrollHeight > document.body.clientHeight;
  var pridat;
  if(hasVScroll === true){
    pridat = 17;
  }
  else{
    pridat = 0;
  }
  var step1 = 450-pridat;
  var step2 = 1000-pridat;
   var step3 = 1000-pridat;
  var step4 = 1000-pridat;
        
 
  if(document.body.offsetWidth <= step1) {  var count = 1; var size = 99.6;  }
  else if(document.body.offsetWidth <= step2) {  var count = 2;  var size = 32; }
  else if(document.body.offsetWidth <= step3) {  var count = 3;  var size = 32; }  
  else if(document.body.offsetWidth <= step4) {  var count = 3;  var size = 32; var nextsize = 27;  }
  else {  var count = 3;   var size = 32; var nextsize = 27; }
    
  // document.getElementById('wrapperin').innerHTML=  document.body.offsetWidth + ' | ' + count;

  var x = document.getElementsByTagName('span');
 
for (var i = 0; i < x.length; i++)
	{  	
 if((x[i].className === 'newmember newmember2 count')||(x[i].className === 'newmember newmember2 countmore')){
   
     x[i].style.position = 'relative';
     x[i].style.top = '0px';
     x[i].style.left = '0px';	
     x[i].style.width  = size + '%';    
  }
  } 
  var lasthi = 0;   
  
  for (var i = 0; i < x.length-1; i++)
	{  	

   if((x[i].className === 'newmember newmember2 count')||(x[i].className === 'newmember newmember2 countmore')){
      x[i+count].style.position = 'absolute';
      var newtop =  x[i].offsetTop +   x[i].offsetHeight + 20 ;  
      var newhi = x[i].offsetHeight+x[i].offsetTop;   
      if(newhi > lasthi) { lasthi = newhi;  }    
	    x[i+count].style.top = newtop+'px';
	    var newleft =  x[i].offsetLeft; 
      x[i+count].style.left = newleft+'px';	 
      
   }
  }
 
  newkatop = newtop -150   ;
  if(document.getElementById('gopartners2'))  {
  var gopartners2 = document.getElementById('gopartners2');
  gopartners2.style.height =  newkatop+'px';
   }
 
 
  
 
    
}

function flagChanges() 
{
 
 
  $("#ChangeLabelDiv").html("<p>Provedli jste ve formuláři změny, které nejsou uložené.</p>");
}

function autoSave(name,user,table){}
/*
  var content = jQuery('textarea[name=' + name + ']').val();
  url='/pages/autoSaver.php?ver=1'+name+'&content='+ content + '&fieldname='+ name + '&table='+ table + '&user='+ user;
 
  if (window.ActiveXObject)
        {
          httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else
        {
          httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange= function () {processRequest(); } ;
        httpRequest.send(null);  
        return false;
 }
 function processRequest(){
  //httpRequest.setRequestHeader("Content-type", "text/html; charset=windows-1250");
  if (httpRequest.readyState == 4)
  {
    if(httpRequest.status == 200)
    {      
  
      semka.innerHTML = httpRequest.responseText;
    }
    else
    {
        alert("Chyba pri nacitani stanky"+ httpRequest.status +":"+ httpRequest.statusText);
    }
  }
}
*/