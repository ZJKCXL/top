<?

namespace String;

class Crypt {
    private static $iv  = 'abchdjedopshedph';

    public static function DES_encrypt($in_data, $in_secret) {
        $result = false;

        $cipher = mcrypt_module_open(MCRYPT_3DES, '', MCRYPT_MODE_ECB, '');
        @mcrypt_generic_init($cipher, $in_secret, '');
        $result = mcrypt_generic($cipher, $in_data);
        mcrypt_generic_deinit($cipher);
        mcrypt_module_close($cipher);

        if (strlen($result)>0)
        {
            $result = base64_encode($result);
        }

        return $result;
    }

    public static function DES_decrypt($in_data, $in_secret) {
        $result = false;

        if (strlen($in_data)>0)
        {
            $in_data = base64_decode($in_data);
        }

        $cipher = mcrypt_module_open(MCRYPT_3DES, '', MCRYPT_MODE_ECB, '');
        @mcrypt_generic_init($cipher, $in_secret, '');
        $result = mdecrypt_generic($cipher, $in_data);
        mcrypt_generic_deinit($cipher);
        mcrypt_module_close($cipher);

        return $result;
    }

    public static function feistel_encrypt($in_text) {
        $result = false;
        if (strlen($in_text)>0)
        {
            for ($i=0;$i<strlen($in_text);$i++)
            {
                $in_byte = ord(substr($in_text,$i,1));
                $elem = pow(-1,$i%2);
                $data = $in_byte + $elem;
                $result .= chr($data);
            }

            $result = base64_encode($result);
        }

        return $result;
    }

    public static function feistel_decrypt($in_text) {
        $result = false;

        if (strlen($in_text)>0) {
            $in_text = base64_decode($in_text);
            for ($i=0;$i<strlen($in_text);$i++) {
                $in_byte = ord(substr($in_text,$i,1));
                $elem = pow(-1,$i%2);
                $data = $in_byte - $elem;
                $result .= chr($data);
            }
        }

        return $result;
    }

    public static function mysql_password($in_text) {
        $result = false;

        if (strlen($in_text)>0) {
            $bin = sha1($in_text, true);
            if ($bin !== false) {
                $hash = sha1($bin);
                if ($hash !== false) {
                    $result = $hash;
                }
            }

        }

        return $result;
    }

    public static function hex2bin($hexdata) {
        $bindata = '';
        for ($i = 0; $i < strlen($hexdata); $i += 2) {
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
        }
        return $bindata;
    }

    public static function pkcs5_pad ($text) {
        $blocksize = 16;
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    public static function pkcs5_unpad($text) {
        $pad = ord($text{strlen($text)-1});
        if ($pad > strlen($text)) {
            return $text;
        }
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) {
            return $text;
        }
        return substr($text, 0, -1 * $pad);
    }

    public static function rijndael_encrypt($str, $in_key) {
        $str = self::pkcs5_pad($str);
        $iv = self::$iv;
        $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
        mcrypt_generic_init($td, $in_key, $iv);
        $encrypted = mcrypt_generic($td, $str);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return bin2hex($encrypted);
    }

    public static function rijndael_decrypt($code, $in_key) {
        $code = self::hex2bin($code);
        $iv = self::$iv;
        $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
        mcrypt_generic_init($td, $in_key, $iv);
        $decrypted = mdecrypt_generic($td, $code);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $ut =  utf8_encode(trim($decrypted));

        return self::pkcs5_unpad($ut);
    }
}



?>