<?

namespace String;

/*

Behemoth 2011-11-01
GID ma presne 64 bytu a navzdy by mel mit

*/

class GID
{
    private $gid;
    private $objectID;
    private $time_stamp;
    private $hash;
    private $rand;
    private static $errcode;

    public final function __construct($forObject = 0,$forceGID = false)
    {
        $this->errcode = new \Kernel\Error(NO_ERR);
        if ($forceGID === false)
        {
            if ($forObject >= 0 && is_numeric($forObject))
            {
                $this->objectID = str_pad($forObject,4,'0',STR_PAD_LEFT);
                list($usec, $sec) = explode(" ", microtime());
                $this->time_stamp = $sec.$usec;
                $this->rand = str_pad(rand(1,99999),5,'0',STR_PAD_LEFT);
                $this->hash = md5($this->objectID."-".$this->time_stamp."-".$this->rand);
                $this->gid = $this->objectID."-".$this->time_stamp."-".$this->rand."-".$this->hash;
            }
            else
            {
                $this->errcode =  new \Kernel\Error(GID_INVALID_PARAMS);
                return false;
            }
        }
        else
        {
            if (self::validateGID($forceGID))
            {
                $this->gid = $forceGID;
                $this->objectID = $this->getObjectID();
                $this->time_stamp = $this->getTimestamp();
                $this->rand = $this->getRand();
                $this->hash = $this->getHash();
            }
            else
            {
                $this->errcode =  new \Kernel\Error(GID_INVALID_PARAMS);
                return false;
            }
        }
    }

    public static function create() {
        $tmp = new GID();
        return $tmp->getGID();
    }

    public static final function loadFromGID($in_gid)
    {
        if (self::validateGID($in_gid))
        {
            return new GID(0,$in_gid);
        }
    }

    public function getObjectID()
    {
        $expl = explode("-",$this->gid);
        if (is_array($expl) && @count($expl)>0)
        {
            return $expl[0];
        }
    }

    public function getTimestamp()
    {
        $expl = explode("-",$this->gid);
        if (is_array($expl) && @count($expl)>1)
        {
            return $expl[1];
        }
    }

    private function getRand()
    {
        $expl = explode("-",$this->gid);
        if (is_array($expl) && @count($expl)>2)
        {
            return $expl[2];
        }
    }

    private function getHash()
    {
        $expl = explode("-",$this->gid);
        if (is_array($expl) && @count($expl)>3)
        {
            return $expl[3];
        }
    }

    public final function getGID()
    {
        if (isset($this->gid))
        {
            return $this->gid;
        }
        else
        {
            $this->errcode = new \Kernel\Error(GID_NO_GID);
            return false;
        }

    }

    public static final function validateGID($inGID)
    {
        $result = false ;

        if (!is_null($inGID) && is_string($inGID)) {
            if (isset($inGID) && strlen($inGID) <= 64 && strlen($inGID) > 16)  {
                $GIDexpl = explode("-",$inGID);
                $result = (md5($GIDexpl[0]."-".$GIDexpl[1]."-".$GIDexpl[2]) == $GIDexpl[3]);
            }
        }

        return $result;
    }

    public function getObjectClass () {
        $result = false;

        if ($this->objectID > 0)
        {
            $query = "SELECT hlpObjectID.objectClass";
            $query .= " FROM (hlpObjectID)";
            $query .= " WHERE 1";
            $query .= " AND hlpObjectID.ID=".$this->objectID;

            $db = new Data();
            $db->doQuery($query);
            if ($db->getNumRows() > 0)
            {
                $row = $db->getFetchedDataLine();
                $result = $row["ID"];
            }
        }

        return $result;
    }

    public function getObjectTable () {
        $result = false;

        if ($this->objectID > 0)
        {
            $query = "SELECT hlpObjectID.objectTable";
            $query .= " FROM (hlpObjectID)";
            $query .= " WHERE 1";
            $query .= " AND hlpObjectID.ID=".$this->objectID;

            $db = new Data();
            $db->doQuery($query);
            if ($db->getNumRows() > 0)
            {
                $row = $db->getFetchedDataLine();
                $result = $row["objectTable"];
            }
        }

        return $result;
    }

    public function __toString()
    {
        return $this->gid;
    }

    public static final function lastError()
    {
        return self::$errcode;
    }

}




?>