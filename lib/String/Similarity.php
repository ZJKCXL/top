<?php
namespace String;

class Similarity {
    const SIMILARITY_THRESHOLD = 0.75;

    public static function getSimiliarity($in_search_word, $in_dictionary_word) {
        $res = similar_text($in_dictionary_word, $in_search_word, $result);

        return $result / 100;
    }
}


?>
