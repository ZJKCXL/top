<?php
namespace String;

class Trigram {

    const MATCH_THRESHOLD = 0.3;

    private static $index = array();

    public static function generateTrigrams($in_word) {
        $result = false;

        if (strlen($in_word) > 0) {

            for ($i = 0; $i < strlen($in_word) + 2; $i++) {
                $gram = $in_word[$i-2].$in_word[$i-1].$in_word[$i];
                if (strlen($gram) < 3) {
                    if ($i < strlen($in_word)) {
                        $gram = str_pad($gram, 3, "_", STR_PAD_LEFT);
                    } else {
                        $gram = str_pad($gram, 3, "_", STR_PAD_RIGHT);
                    }
                }

                $result[] = $gram;
            }

        }

        return $result;
    }
}
?>
