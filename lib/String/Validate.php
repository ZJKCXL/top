<?php

namespace String;

class Validate
{

    public static function string($in_string, $in_type = "numbers", $in_case_sensitive = true)
    {
        if (strlen($in_string) <= 0) {
            return false;
        }

        switch ($in_type) {
            case "numbers":
                $str = "0123456789";
                $eregstr = "^[" . $str . "]{1,}$";
                break;
            case "integer":
                $eregstr = "^([-]|())([01234567890]{1,})$";
                break;
            case "float":
                $eregstr = "^([-]|())([0123456789]{1,})(()|(([\054])([0123456789]{1,})))$";
                break;
            case "safeChars":
                $str = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
                $eregstr = "^[" . $str . "]{1,}$";
                break;
            case "alphabetLowercase":
                $str = "abcdefghijklmnopqrstuvwxyzáčďéěíňóřšťúůťýžüöäë ";
                $eregstr = "^[" . $str . "]{1,}$";
                break;
            case "alphabetUppercase":
                $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽÜÖÄËß ";
                $eregstr = "^[" . $str . "]{1,}$";
                break;
            case "alphabet":
                $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáčďéěíňóřšťúůťýžÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽüöäëÜÖÄËß ";
                $eregstr = "^[" . $str . "]{1,}$";
                break;
            case "alphanumeric":
                $str = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáčďéěíňóřšťúůťýžÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽüöäëÜÖÄËß ";
                $eregstr = "^[" . $str . "]{1,}$";
                break;
            case "extended":
                $str = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáčďéěíňóřšťúůťýžÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽüöäëÜÖÄËß\\\\\n\r\t\040\041\042\043\044\045\046\047\050\051\052\053\054\055\056\057\072\073\074\075\076\077\092\100\134\137\140\173\175´";
                $eregstr = "^[" . $str . "]{1,}$";
                break;
            case "phoneInternational":
                $eregstr = "^((([+][1-9][0-9]{2}([ ]{0,1}))|([0]{2}([ ]{0,1})[1-9][0-9]{2}([ ]{0,1})))|())([1-9]{1}[0-9]{2}([ ]{0,1})[0-9]{3}([ ]{0,1})[0-9]{3})$";
                break;
            case "phone":
                $eregstr = "^[+]{0,1}[0-9 ]+$";
                break;
            case "url":
                $eregstr = "^([htt]+(p|s))|[ftp]+[:]\/\/[a-zA-Z0-9]+([-_\.]?[a-zA-Z0-9])*\.[a-zA-Z]{2,4}(\/{1}[-_~&=\?\.a-z0-9]*)*$";
                break;
            case "email":
                $eregstr = "^[a-z0-9]+[a-z0-9\._-]*[a-z0-9]+@[a-z0-9]+[a-z0-9\._-]*[a-z0-9]+\.[a-z]{2,4}$";
                break;
            case "us_date":
            case "en_date":
                $eregstr = "^(([0-9]{4})([-])((0[1-9])|(1[012]))([-])((0[1-9])|([12][0-9])|(3[01])))$";
                break;
            case "us_datetime":
            case "en_datetime":
                $eregstr = "^((([0-9]{4})([-])((0[1-9])|(1[012]))([-])((0[1-9])|([12][0-9])|(3[01])))([ ]{1})(((0[0-9])|(1[0-9])|(2[0-3]))([:])([0-5][0-9])([:])([0-5][0-9])))$";
                break;
            case "cz_date":
            case "cs_date":
                $eregstr = "^(((0[1-9]|[12][0-9]|3[01])([.])(0[13578]|10|12)([.])([1-2][0,9][0-9][0-9]))|(([0][1-9]|[12][0-9]|30)([.])(0[469]|11)([.])([1-2][0,9][0-9][0-9]))|((0[1-9]|1[0-9]|2[0-8])([.])(02)([.])([1-2][0,9][0-9][0-9]))|((29)([.])(02)([.])([02468][048]00))|((29)([.])(02)([.])([13579][26]00))|((29)([.])(02)([.])([0-9][0-9][0][48]))|((29)([.])(02)([.])([0-9][0-9][2468][048]))|((29)([.])(02)([.])([0-9][0-9][13579][26])))$";
                break;
            case "time":
                $eregstr = "^(((0[0-9])|(1[0-9])|(2[0-3]))([:])([0-5][0-9]))$";
                break;

            default:
                break;
        }

        if (isset($eregstr)) {
            if (!$in_case_sensitive) {
                if (eregi($eregstr, $in_string)) {
                    return true;
                }
            } else {
                if (ereg($eregstr, $in_string)) {
                    return true;
                }
            }
        }

        return false;
    }

}
