<?php
namespace Tools;

class Calendar {

    const RUNNING_WEEK_COUNT = 4;

    public static function getRunningCalendar($in_date, $in_weeks = self::RUNNING_WEEK_COUNT) {
        $result = array();

        $first_day = strtotime(\Tools\Week::getWeekStart($in_date)." 05:00:00");
        $last_day = strtotime(\Tools\Date::addWeek(\Tools\Week::getWeekEnd($in_date),$in_weeks)." 05:00:00");
        for ($i = $first_day; date("Y-m-d",$i) <= date("Y-m-d",$last_day); $i += 24*3600) {
            $result[date("Y-m-d", $i)]["day_of_week"] = date("N", $i);
            $result[date("Y-m-d", $i)]["day_of_month"] = date("j", $i);
        }

        return $result;
    }

    public static function getCalendarFrom($in_date, $in_weeks = self::RUNNING_WEEK_COUNT) {
        $result = array();

        $first_day = strtotime($in_date." 05:00:00");
        $last_day = strtotime(\Tools\Date::addWeek($in_date,$in_weeks)." 05:00:00");
        for ($i = $first_day; date("Y-m-d",$i) <= date("Y-m-d",$last_day); $i += 24*3600) {
            $result[date("Y-m-d", $i)]["day_of_week"] = date("N", $i);
            $result[date("Y-m-d", $i)]["day_of_month"] = date("j", $i);
        }

        return $result;
    }

    public static function getCalendarInterval($from_date, $to_date) {
        $result = array();

        $first_day = strtotime($from_date." 05:00:00");
        $last_day = strtotime($to_date." 05:00:00");
        for ($i = $first_day; date("Y-m-d",$i) <= date("Y-m-d",$last_day); $i += 24*3600) {
            $result[date("Y-m-d", $i)]["day_of_week"] = date("N", $i);
            $result[date("Y-m-d", $i)]["day_of_month"] = date("j", $i);
        }

        return $result;
    }

    public static function dayCountToText($in_day_count) {
        $result = false;

        switch ($in_day_count) {
            case 1:
                $result = TXT_DEAL_DETAIL_POPUP_NEW_07;
                break;
            case 7:
                $result = TXT_DEAL_DETAIL_POPUP_NEW_08;
                break;
            case 14:
                $result = "dva týdny";
                break;
            case 30:
                $result = TXT_DEAL_DETAIL_POPUP_NEW_09;
                break;
            case 90:
                $result = TXT_DEAL_DETAIL_POPUP_NEW_10;
                break;
            case 180:
                $result = TXT_DEAL_DETAIL_POPUP_NEW_11;
                break;
            case 360:
                $result = TXT_DEAL_DETAIL_POPUP_NEW_12;
                break;
        }

        return $result;
    }
}
?>
