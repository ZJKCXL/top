<?php
namespace Tools;

class Color {

    /**
    * Convert a hexadecimal color in RGB
    * @param string $hex
    * @return array
    */
    public static function hexToHsl($hex){
        list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
        return self::rgbToHsl($r, $g, $b);
    }

    public static function hexToHsv($hex){
        list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
        return self::rgbToHsv($r, $g, $b);
    }

    /**
    * Convert a RGB color in its HSL value
    * @param int $r red
    * @param int $g green
    * @param int $b blue
    * @return array
    */
    public static function rgbToHsl($r, $g, $b)
    {
        $r /= 255;
        $g /= 255;
        $b /= 255;

        $max = max($r, $g, $b);
        $min = min($r, $g, $b);

        $h = 0;
        $l = ($max + $min) / 2;
        $d = $max - $min;

        if ($d == 0) {
            $h = $s = 0; // achromatic
        } else {
            $s = $d / (1 - abs(2 * $l - 1));

            switch ($max) {
                case $r:
                    $h = 60 * fmod((($g - $b) / $d), 6);
                    if ($b > $g) {
                        $h += 360;
                    }
                    break;

                case $g:
                    $h = 60 * (($b - $r) / $d + 2);
                    break;

                case $b:
                    $h = 60 * (($r - $g) / $d + 4);
                    break;
            }
        }
        return array('h' => round($h, 2), 's' => round($s, 2), 'l' => round($l, 2));
    }

    public static function rgbToHsv($R, $G, $B) {
        // Convert the RGB byte-values to percentages
        $R = ($R / 255);
        $G = ($G / 255);
        $B = ($B / 255);

        // Calculate a few basic values, the maximum value of R,G,B, the
        //   minimum value, and the difference of the two (chroma).
        $maxRGB = max($R, $G, $B);
        $minRGB = min($R, $G, $B);
        $chroma = $maxRGB - $minRGB;

        // Value (also called Brightness) is the easiest component to calculate,
        //   and is simply the highest value among the R,G,B components.
        // We multiply by 100 to turn the decimal into a readable percent value.
        $computedV = 100 * $maxRGB;

        // Special case if hueless (equal parts RGB make black, white, or grays)
        // Note that Hue is technically undefined when chroma is zero, as
        //   attempting to calculate it would cause division by zero (see
        //   below), so most applications simply substitute a Hue of zero.
        // Saturation will always be zero in this case, see below for details.
        if ($chroma == 0)
            return array(0, 0, $computedV);

        // Saturation is also simple to compute, and is simply the chroma
        //   over the Value (or Brightness)
        // Again, multiplied by 100 to get a percentage.
        $computedS = 100 * ($chroma / $maxRGB);

        // Calculate Hue component
        // Hue is calculated on the "chromacity plane", which is represented
        //   as a 2D hexagon, divided into six 60-degree sectors. We calculate
        //   the bisecting angle as a value 0 <= x < 6, that represents which
        //   portion of which sector the line falls on.
        if ($R == $minRGB)
            $h = 3 - (($G - $B) / $chroma);
        elseif ($B == $minRGB)
            $h = 1 - (($R - $G) / $chroma);
        else // $G == $minRGB
            $h = 5 - (($B - $R) / $chroma);

        // After we have the sector position, we multiply it by the size of
        //   each sector's arc (60 degrees) to obtain the angle in degrees.
        $computedH = 60 * $h;

        return array("h" => $computedH, "s" => $computedS, "v" => $computedV);
    }

    public static function doSortHSL(&$colors) {
        foreach ($colors as &$color) {
            $color['hsl'] = self::hexToHsl($color['color']);
        }

        usort($colors, function ($a, $b) {
            //Compare the hue when they are in the same "range"
            if(!self::huesAreinSameInterval($a['hsl']['h'],$b['hsl']['h'])){
                if ($a['hsl']['h'] < $b['hsl']['h'])
                    return -1;
                if ($a['hsl']['h'] > $b['hsl']['h'])
                    return 1;
            }
            if ($a['hsl']['l'] < $b['hsl']['l'])
                return 1;
            if ($a['hsl']['l'] > $b['hsl']['l'])
                return -1;
            if ($a['hsl']['s'] < $b['hsl']['s'])
                return -1;
            if ($a['hsl']['s'] > $b['hsl']['s'])
                return 1;
            return 0;
        });

        return $colors;
    }

    public static function doSortHSV(&$colors) {
        foreach ($colors as &$color) {
            $color["hsv"] = self::hexToHsv($color['color']);
        }
        usort($colors, function ($a, $b) {
            //Compare the hue when they are in the same "range"
            if(!self::huesAreinSameInterval($a['hsl']['h'],$b['hsl']['h'])){
                if ($a['hsl']['h'] < $b['hsl']['h'])
                    return -1;
                if ($a['hsl']['h'] > $b['hsl']['h'])
                    return 1;
            }
            if ($a['hsl']['v'] < $b['hsl']['v'])
                return 1;
            if ($a['hsl']['v'] > $b['hsl']['v'])
                return -1;
            if ($a['hsl']['s'] < $b['hsl']['s'])
                return -1;
            if ($a['hsl']['s'] > $b['hsl']['s'])
                return 1;
            return 0;
        });

        return $colors;
    }

    public static function doSortStep(&$colors) {
        foreach ($colors as &$color) {
            list($r, $g, $b) = sscanf($color["color"], "#%02x%02x%02x");
            $color["step"] = self::rgbStep($r, $g, $b, 8);
        }

        usort($colors, function ($a, $b) {
             if ($a["step"] < $b["step"]) {
                 return -1;
             } else {
                 return 1;
             }
        });

        return $colors;
    }

    public static function rgbStep($r, $g, $b, $repeat = 1) {
        $lum = sqrt(0.241 * $r + 0.691 * $g + 0.608 * $b);

        $hsv = self::rgbToHsv($r, $g, $b);

        $res = array();
        $res["h"] = (int)($hsv['h'] * $repeat);
        $res["l"] = (int)($lum * $repeat);
        $res["v"] = (int)($hsv['v'] * $repeat);

        $result = $res["v"] * 100000 + $res["l"] * 1000 + $res["h"];

        return $result;
    }

    /**
    * Check if two hues are in the same given interval
    * @param float $hue1
    * @param float $hue2
    * @param int $interval
    * @return bool
    */
    public static function huesAreinSameInterval($hue1, $hue2, $interval = 30){
        return (round(($hue1 / $interval), 0, PHP_ROUND_HALF_DOWN) === round(($hue2 / $interval), 0, PHP_ROUND_HALF_DOWN));
    }

}
?>
