<?php
namespace Tools;

class Math {
    /**
    * generates a float rounded up to neareast fraction (or number)
    *
    * @param float $in_number
    * @param float $round_to_nearest
    * @returns float
    */
    public static function roundTo($in_number, $round_to_nearest) {
        $result = $in_number;
        if ($round_to_nearest != 0) {
            $result = round($in_number/$round_to_nearest, 0)* $round_to_nearest;
        }
        return $result;
    }

    public static function parseFloat($in_string) {
        $result = false;

        $num = trim($in_string);
        $num = str_replace(",",".", $num);
        if (strlen($num) > 0) {
            $result = floatval($num);
        }

        return $result;
    }
}
?>
