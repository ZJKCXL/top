<?

namespace Tools;

define("NOTIFICATION_TEMPLATE_HEADER","header");
define("NOTIFICATION_TEMPLATE_HEADER_STUB","<%HEADER_TEXT%>");
define("NOTIFICATION_TEMPLATE_HEADER_APP_STUB","<%APP_NAME%>");
define("NOTIFICATION_TEMPLATE_LINK_BASE_URL_STUB","<%BASE_URL%>");
define("NOTIFICATION_TEMPLATE_LINK_LINK_STUB","<%LINK%>");
define("NOTIFICATION_TEMPLATE_FOOTER","footer");
define("NOTIFICATION_TR_FOOTER","footer_tr");

define("NOTIFICATION_MEDIUM_MAIL",1);
define("NOTIFICATION_MEDIUM_SMS",2);
define("NOTIFICATION_MEDIUM_GTALK",3);

define("NOTIFICATION_TYPE_TICKET_NEW","ticket-new");
define("NOTIFICATION_TYPE_TICKET_NEW_CONFIRM","ticket-new-confirm");
define("NOTIFICATION_TYPE_TICKET_ACCEPTED","ticket-accepted");
define("NOTIFICATION_TYPE_TICKET_CLOSED","ticket-closed");
define("NOTIFICATION_TYPE_TICKET_COMMENT","ticket-comment");
define("NOTIFICATION_TYPE_TICKET_POSTPONED","ticket-postponed");
define("NOTIFICATION_TYPE_TICKET_REOPENED","ticket-reopened");
define("NOTIFICATION_TYPE_TICKET_PUSH_AGENT","ticket-push-agent");
define("NOTIFICATION_TYPE_TICKET_PUSH_AGENT_USER","ticket-push-agent-user");
define("NOTIFICATION_TYPE_TICKET_PUSH_AGENT_PREV","ticket-push-agent-prev");
define("NOTIFICATION_TYPE_TICKET_PUSH_FORCE","ticket-push-force");
define("NOTIFICATION_TYPE_TICKET_PUSH_FORCE_USER","ticket-push-force-user");
define("NOTIFICATION_TYPE_USER_RESET_REQUEST","user-reset-request");
define("NOTIFICATION_TYPE_TICKET_SKILL_CHANGE","ticket-skill-change");
define("NOTIFICATION_TYPE_TICKET_SKILL_CHANGE_PREV","ticket-skill-change-prev");
define("NOTIFICATION_TYPE_TICKET_TAKEOVER_USER","ticket-takeover-user");
define("NOTIFICATION_TYPE_TICKET_TAKEOVER_PREV","ticket-takeover-prev");


define("NOTIFICATION_PART_LINK","link");

class Notification {

    protected static $column_binding = array("id"=>"ID", "name" => "NName", "medium" => "NMedium", "header" => "NHeader", "footer" => "NFooter", "body" => "NBody", "lang" => "NLang", "subject" => "NSubject");

    private static function header_get_text($in_notification_type, $in_notification_medium = NOTIFICATION_MEDIUM_MAIL)
    {
        $result = "";

        $cells["header"] = self::$column_binding["header"];
        $params[self::$column_binding["name"]] = $in_notification_type;
        $params[self::$column_binding["medium"]] = $in_notification_medium;

        $tab = new Table("tblNotification");

        $data = $tab->recordSearch($cells,$params,false,false,false,false,false,false,$explicit_sql);

        if ($data !== false && @is_array($data) && @count($data) > 0)
        {
            foreach ($cells as $cidx => $col)
            {
                $result[$cidx] = $data[0][$col];
            }

            $result = $result["header"];
        }

        return $result;
    }

    private static function header_get($in_notification_type, $in_notification_medium = NOTIFICATION_MEDIUM_MAIL)
    {

        $result = "";

        $cells["header"] = self::$column_binding["header"];
        $params[self::$column_binding["name"]] = NOTIFICATION_TEMPLATE_HEADER;
        $params[self::$column_binding["medium"]] = $in_notification_medium;

        $tab = new Table("tblNotification");

        $data = $tab->recordSearch($cells,$params);

        if ($data !== false && @is_array($data) && @count($data) > 0)
        {
            foreach ($cells as $cidx => $col)
            {
                $result[$cidx] = $data[0][$col];
            }

            $result = $result["header"];
            if (strlen($result) > 0)
            {
                $header_text = self::header_get_text($in_notification_type, $in_notification_medium);
                if (strlen($header_text)>0)
                {
                    $result = str_replace(NOTIFICATION_TEMPLATE_HEADER_STUB,$header_text,$result);
                    $result = str_replace(NOTIFICATION_TEMPLATE_HEADER_APP_STUB,APP_NAME,$result);
                }
            }
        }

        return $result;
    }

    private static function footer_get($in_notification_medium = NOTIFICATION_MEDIUM_MAIL)
    {
        $result = "";

        $cells["footer"] = self::$column_binding["footer"];
        $params[self::$column_binding["name"]] = NOTIFICATION_TEMPLATE_FOOTER;
        $params[self::$column_binding["medium"]] = $in_notification_medium;

        $tab = new Table("tblNotification");

        $data = $tab->recordSearch($cells,$params);

        if ($data !== false && @is_array($data) && @count($data) > 0)
        {
            foreach ($cells as $cidx => $col)
            {
                $result[$cidx] = $data[0][$col];
            }

            $result = $result["footer"];
        }

        return $result;
    }

    private static function part_get($in_part, $in_notification_medium = NOTIFICATION_MEDIUM_MAIL, $in_params = false)
    {
        $result = "";

        $cells["body"] = self::$column_binding["body"];
        $params[self::$column_binding["name"]] = $in_part;
        $params[self::$column_binding["medium"]] = $in_notification_medium;

        $tab = new Table("tblNotification");

        $data = $tab->recordSearch($cells,$params);

        if ($data !== false && @is_array($data) && @count($data) > 0)
        {
            foreach ($cells as $cidx => $col)
            {
                $result[$cidx] = $data[0][$col];
            }

            $result = $result["body"];
        }

        if (strlen($result)>0)
        {
            switch ($in_part)
            {
                case NOTIFICATION_PART_LINK:
                    $result = str_replace(NOTIFICATION_TEMPLATE_LINK_BASE_URL_STUB,BASE_URL,$result);
                    $result = str_replace(NOTIFICATION_TEMPLATE_LINK_LINK_STUB,$in_params["link"],$result);
                    break;
            }
        }

        return $result;
    }

    private static function body_get($in_notification_type, $in_notification_medium = NOTIFICATION_MEDIUM_MAIL)
    {
        $result = "";

        $cells["body"] = self::$column_binding["body"];
        $params[self::$column_binding["name"]] = $in_notification_type;
        $params[self::$column_binding["medium"]] = $in_notification_medium;

        $tab = new Table("tblNotification");

        $data = $tab->recordSearch($cells,$params,false,false,false,false,false,false,$explicit_sql);

        if ($data !== false && @is_array($data) && @count($data) > 0)
        {
            foreach ($cells as $cidx => $col)
            {
                $result[$cidx] = $data[0][$col];
            }

            $result = $result["body"];
        }

        return $result;
    }

    private static function subject_get($in_notification_type, $in_notification_medium = NOTIFICATION_MEDIUM_MAIL)
    {
        $result = "";

        $cells["subject"] = self::$column_binding["subject"];
        $params[self::$column_binding["name"]] = $in_notification_type;
        $params[self::$column_binding["medium"]] = $in_notification_medium;

        $tab = new Table("tblNotification");

        $data = $tab->recordSearch($cells,$params,false,false,false,false,false,false,$explicit_sql);

        if ($data !== false && @is_array($data) && @count($data) > 0)
        {
            foreach ($cells as $cidx => $col)
            {
                $result[$cidx] = $data[0][$col];
            }

            $result = $result["subject"];
        }

        return $result;
    }

    public function notify($in_notification_type, $in_notification_medium = NOTIFICATION_MEDIUM_MAIL, $in_params = false)
    {
        $result = false;

        $subject = self::subject_get($in_notification_type, $in_notification_medium);
        $body = self::body_get($in_notification_type, $in_notification_medium);
        $header = self::header_get($in_notification_type,$in_notification_medium);
        $footer = self::footer_get($in_notification_medium);

        if (strlen($body) > 0 && strlen($body) > 0)
        {
            $target = array();

            if (isset($in_params["data"]) && is_array($in_params["data"]) && @count($in_params["data"]))
            {
                foreach ($in_params["data"] as $data_idx => $data_val)
                {
                    $body = str_replace("<%".$data_idx."%>",$data_val,$body);
                    $subject = str_replace("<%".$data_idx."%>",$data_val,$subject);
                }
            }

            if (isset($in_params["link"]) &&  strlen($in_params["link"])>0)
            {
                $link = self::part_get(NOTIFICATION_PART_LINK,$in_notification_medium, array("link" => $in_params["link"]));
                if (strlen($link) > 0)
                {
                    $body .= $link;
                }
            }

            $body = $header.$body.$footer;

            if (isset($in_params["recepients"]) && is_array($in_params["recepients"]) && @count($in_params["recepients"]))
            {
                foreach ($in_params["recepients"] as $recepient)
                {
                    switch ($in_notification_medium)
                    {
                        case NOTIFICATION_MEDIUM_MAIL:
                            $user_email = User::get_email($recepient);
                            if ($user_email != false)
                            {
                                $target[] = $user_email;
                            }
                            break;
                    }
                }
            }

            if (isset($target) && is_array($target) && @count($target))
            {
                switch ($in_notification_medium)
                {
                    case NOTIFICATION_MEDIUM_MAIL:
                        $result = Mail::do_send($target, $subject, $body);
                        break;
                }
            }
        }

        return $result;
    }
}


?>
