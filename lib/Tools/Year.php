<?php
namespace Tools;

class Year {
    public static function isLeap($in_year) {
        $result = false;

        if (intval($in_year) > 0) {
            $date = $in_year."-01-01";
            $stmp = strtotime($date);
            if ($stmp > 0) {
                $result = (date("L", $stmp) == 1);
            }
        }

        return $result;
    }
}
?>
