<?php

class Globals
{
  public static $META_DESC_LENGTH_IDEAL = 160; 
  public static $META_KEYWORDS_LENGTH_IDEAL = 160; 
  public static $META_TITLE_LENGTH_IDEAL = 60; 
  public static $META_DESC_LENGTH = 200; 
  public static $META_KEYWORDS_LENGTH = 200; 
  public static $META_TITLE_LENGTH = 70; 
  public static $GLOBAL_DASHBOARD = '17QnvI5PNjPxLF6ssw0ecyBLJpRjrCUQn';
  public static $GLOBAL_HOME = "/data/www/23478/topof_cz/www/";   //puvodni globalgal
  public static $GLOBAL_WEB_PATH = "/data/www/23478/topof_cz/www";
  public static $GLOBAL_TEMP_DIR = '';   
  public static $GLOBAL_BASE_URL = 'http://www.topof.cz/';  
  public static $GLOBAL_MAILZGO_TRAP = 1;
  public static $GLOBAL_CONTACT_EMAIL = "kriz@byznysprospolecnost.cz";
  public static $GLOBAL_CONTACT_HIM = "Milanovi Křížovi";
  public static $GLOBAL_SHOW_ERRORZ = 1;
  public static $GLOBAL_SQL_FILE = '/sql/db19.php'; 
  public static $GLOBAL_JSPDF_PATH = "/eval/jobs/jsPDF/";
  public static $GLOBAL_LOCK_BY_CONTEST = 0;  // 1 zamceno/ 0 odemceno pri soutezi
  public static $GLOBAL_LOCK_BY_JURY = 0;  // 1 zamceno/ 0 odemceno pri hodnoceni
  public static $GLOBAL_DIZ_YEAR = 2019;  
  public static $GLOBAL_LAST_YEAR = 2018;   
 }
  // pouziti pak Globals::$GLOBAL_LAST_YEAR
?>