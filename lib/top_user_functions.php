<?php



function feistel_encrypt($in_text)
{
	$j=0;
	$result = "";
	for ($i=0;$i<strlen($in_text);$i++)
	{
		$in_byte = ord(substr($in_text,$i,1));
		$elem = pow(-1,$i%2);
		$data = $in_byte + $elem;
		$result .= chr($data);
	}
	$result = base64_encode($result);
	return $result;
	exit;
}

function feistel_decrypt($in_text)
{
	$j=0;
	$result = "";
	$in_text = base64_decode($in_text);
	for ($i=0;$i<strlen($in_text);$i++)
	{
		$in_byte = ord(substr($in_text,$i,1));
		$elem = pow(-1,$i%2);
		$data = $in_byte - $elem;
		$result .= chr($data);
	}
	return $result;
	exit;
}

function user_logged_in()
{
	global $topcookie;

	$in_cookie = $_COOKIE[$topcookie["name"]];

	$result = false;

	if (strlen($in_cookie)>0)
	{
		$decoded_cookie = feistel_decrypt($in_cookie);
		$exploded_cookie = explode(";",$decoded_cookie);
		$result = ($exploded_cookie[0] == md5($_SERVER['HTTP_USER_AGENT']));

	}

	return $result;
}

function old_user_logged_in()
{
	global $oldtopcookie;

	$in_cookie = $_COOKIE[$oldtopcookie["name"]];

	$result = false;

	if (strlen($in_cookie)>0)
	{
		$decoded_cookie = feistel_decrypt($in_cookie);
		$exploded_cookie = explode(";",$decoded_cookie);
		$result = ($exploded_cookie[0] == md5($_SERVER['HTTP_USER_AGENT']));

	}

	return $result;
}

function user_login($user_name, $user_pass)
{
	$result = false;
	global $conn;
	global $topcookie;
	$link= @mysql_connect($conn["host"],$conn["user"],$conn["pass"]);
	if (!$link)
	{
		$link = @mysql_connect($conn["host"],$conn["user2"],$conn["pass2"]);
	}
	if (!$link)
	{
		echo "Connection pool full";
		return false;
	}

	$db=mysql_select_db($conn["db"], $link);

	if (!$db)
	{
		return false;
	}

   $query = "Select users.* from users where nickname = '".$user_name."' and fanpass ='".$user_pass."'  ";

	$res = @mysql_query($query);
	if ($res && @mysql_num_rows($res)>0)
	{
		$resarr = @mysql_fetch_array($res);
		$agent =$_SERVER['HTTP_USER_AGENT'];
		$agent = md5($agent);


		$topcookie_data = $agent.";".$resarr["ID"].";".$resarr["fanemail"].";".$resarr["company"];
		$topcookie_data = feistel_encrypt($topcookie_data);
		if (strlen($topcookie_data)>0)
		{
			$result = @setcookie($topcookie["name"],$topcookie_data,null,$topcookie["path"],$topcookie["domain"]);
			mysql_query("Update users Set lastlogin =".time()." Where ID = ".$resarr["ID"]);
		}
	}
	return $result;
}

function old_user_login($user_name, $user_pass)
{
	$result = false;
	global $conn;
	global $oldtopcookie;
	$link= @mysql_connect($conn["host"],$conn["user"],$conn["pass"]);
	if (!$link)
	{
		$link = @mysql_connect($conn["host"],$conn["user2"],$conn["pass2"]);
	}
	if (!$link)
	{
		echo "Connection pool full";
		return false;
	}

	$db=mysql_select_db($conn["db"], $link);

	if (!$db)
	{
		return false;                    
	}

     $query = "Select users2018.* from users2018 where nickname = '".$user_name."' and fanpass ='".$user_pass."'  ";

	$res = @mysql_query($query);
	if ($res && @mysql_num_rows($res)>0)
	{
		$resarr = @mysql_fetch_array($res);
		$agent =$_SERVER['HTTP_USER_AGENT'];       
		$agent = md5($agent);


		$oldtopcookie_data = $agent.";".$resarr["ID"].";".$resarr["fanemail"].";".$resarr["company"];
		$oldtopcookie_data = feistel_encrypt($oldtopcookie_data);
		if (strlen($oldtopcookie_data)>0)
		{
			$result = @setcookie($oldtopcookie["name"],$oldtopcookie_data,null,$oldtopcookie["path"],$oldtopcookie["domain"]);
			mysql_query("Update users2018 Set lastlogin =".time()." Where ID = ".$resarr["ID"]);
		}
	}
	return $result;
}


function user_logout()
{
	$result = false;

 global $topcookie;

     $in_cookie = $_COOKIE[$topcookie["name"]];

	if (strlen($in_cookie)>0)
	{
   $result = setcookie($topcookie["name"],false,time() - 3600,$topcookie["path"],$topcookie["domain"]);

	}
 
	return $result;
}

function old_user_logout()
{
	$result = false;

 global $oldtopcookie;

    $in_cookie = $_COOKIE[$oldtopcookie["name"]];

	if (strlen($in_cookie)>0)
	{
   $result = setcookie($oldtopcookie["name"],false,time() - 3600,$oldtopcookie["path"],$oldtopcookie["domain"]);

	}
 
	return $result;
}

function get_user_id()
{
	global $topcookie;

  $in_cookie = $_COOKIE[$topcookie["name"]];

	if (isset($in_cookie) && $in_cookie != "")
	{
		$decoded_cookie = feistel_decrypt($in_cookie);
		$exploded_cookie = explode(";",$decoded_cookie);
		return $exploded_cookie[1];
	}
	else
	{
		return false;
	}
}

function old_get_user_id()
{
	global $oldtopcookie;

  $in_cookie = $_COOKIE[$oldtopcookie["name"]];

	if (isset($in_cookie) && $in_cookie != "")
	{
		$decoded_cookie = feistel_decrypt($in_cookie);
		$exploded_cookie = explode(";",$decoded_cookie);
		return $exploded_cookie[1];
	}
	else
	{
		return false;
	}
}

function get_user_name($in_user_id)
{
	global $conn;

	$result = false;

	return $result;
}

function get_user_level()
{
	global $topcookie;

	$in_cookie = $_COOKIE[$topcookie["name"]];

	if (isset($in_cookie) && $in_cookie != "")
	{
		$decoded_cookie = feistel_decrypt($in_cookie);
		$exploded_cookie = explode(";",$decoded_cookie);
		return $exploded_cookie[2];
	}
	else
	{
		return false;
	}
}

function get_user_level_name()
{
	global $conn;

	$result = false;

	return $result;
}

function get_user_firm()
{
	global $topcookie;

	$in_cookie = $_COOKIE[$topcookie["name"]];

	if (isset($in_cookie) && $in_cookie != "")
	{
		$decoded_cookie = feistel_decrypt($in_cookie);
		$exploded_cookie = explode(";",$decoded_cookie);
		return $exploded_cookie[3];
	}
	else
	{
		return false;
	}
}

function get_user_name_by_id($in_id, $reversed = false, $separator = ", ")
{
	global $conn;

	$link=@mysql_connect($conn["host"],$conn["user"],$conn["pass"]);
	if (!$link)
	{
		return false;
	}

	$db=mysql_select_db($conn["db"], $link);

	if (!$db)
	{
		return false;
	}

	$query = "Select USurname, UName from tblwebuser where ID=".$in_id;
	$res = @mysql_query($query);

	if ($res && @mysql_num_rows($res)>0)
	{
		$resarr = @mysql_fetch_array($res);
		if ($reversed)
		{
			return $resarr["USurname"].$separator.$resarr["UName"];
		}
		else
		{
			return $resarr["UName"].$separator.$resarr["USurname"];
		}
	}
	else
	{
		return false;
	}

}


?>
